#include "RegionRepository.h"
#include "helpers.h"

namespace core {
void RegionRepository::load(cereal::XMLInputArchive &ar) {
  ar.startNode();
  while (ar.getNodeName() != nullptr) {
    if (strcmp(ar.getNodeName(), "region") == 0) {
      Region region;
      ar(region);
      regions.push_back(region);
    }
    else {
      throw Exception("Unexpected token.");
    }
  }
  ar.finishNode();
}
std::vector<Region> RegionRepository::fetch_all() const {
  return regions;
}
void RegionRepository::insert(Region &region) {
  regions.push_back(region);
}
}
