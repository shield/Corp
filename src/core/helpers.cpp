#include "helpers.h"

#include <boost/random.hpp>
#include <ctime>

unsigned int core::random_between(unsigned int start, unsigned int end) {
  static boost::mt19937 rand_generator(static_cast<const uint32_t &>(std::time(nullptr)));

  boost::uniform_int<> range(start, end);
  boost::variate_generator<boost::mt19937 &, boost::uniform_int<>>
      dice(rand_generator, range);

  return (unsigned int) dice();
}
