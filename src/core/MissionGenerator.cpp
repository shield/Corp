#include "MissionGenerator.h"
#include "helpers.h"
#include "messages.h"
#include "rules.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/random.hpp>
#include <fstream>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/math/distributions/binomial.hpp>

using namespace std;
using boost::math::binomial;

namespace fs = boost::filesystem;

namespace core {

MissionGenerator::MissionGenerator(spRegionRepository &region_repository) {
  MissionGenerator::region_repository = region_repository;
}

string MissionGenerator::uuid_define() {

  boost::uuids::random_generator generator;
  boost::uuids::uuid uuid1 = generator();

  return boost::uuids::to_string(uuid1);
}

unsigned int MissionGenerator::roll_dice_mission() {

  static boost::mt19937 randGen(static_cast<const uint32_t &>(std::time(nullptr)));

  boost::uniform_int<> zero_to_eight(0, 8);
  boost::variate_generator<boost::mt19937 &, boost::uniform_int<> > diceMission(
      randGen, zero_to_eight);

  return diceMission();
}

unsigned int MissionGenerator::roll_dice_mission_alternate() {

  static boost::mt19937 randGen(static_cast<const uint32_t &>(std::time(nullptr)));

  boost::uniform_int<> zero_to_one(0, 1);
  boost::variate_generator<boost::mt19937 &, boost::uniform_int<> >
      diceMissionAlternate(randGen, zero_to_one);

  return diceMissionAlternate();
}

unsigned int MissionGenerator::roll_dice_mission_speech() {

  static boost::mt19937 randGen(static_cast<const uint32_t &>(std::time(nullptr)));

  boost::uniform_int<> zero_to_two(0, 2);
  boost::variate_generator<boost::mt19937 &, boost::uniform_int<> >
      diceMissionSpeech(randGen, zero_to_two);

  return diceMissionSpeech();
}

unsigned int MissionGenerator::roll_dice_super_mission() {

  static boost::mt19937 randGen(static_cast<const uint32_t &>(std::time(nullptr)));

  boost::uniform_int<> zero_to_hundred(0, 100);
  boost::variate_generator<boost::mt19937 &, boost::uniform_int<> >
      diceSuperMission(randGen, zero_to_hundred);

  return diceSuperMission();
}

unsigned int MissionGenerator::roll_dice_dateTime() {

  static boost::mt19937 randGen(static_cast<const uint32_t &>(std::time(nullptr)));

  boost::uniform_int<> zero_to_hundred(1, 7);
  boost::variate_generator<boost::mt19937 &, boost::uniform_int<> >
      diceDateTime(randGen, zero_to_hundred);

  return diceDateTime();
}

unsigned int MissionGenerator::roll_dice_difficulty(int level_max) {

  static boost::mt19937 randGen(static_cast<const uint32_t &>(std::time(nullptr)));

  boost::uniform_int<> one_to_max(1, level_max);
  boost::variate_generator<boost::mt19937 &, boost::uniform_int<> >
      diceDifficulty(randGen, one_to_max);

  return diceDifficulty();
}

string MissionGenerator::determine_pronoun_preposition(string subject,
                                                       bool upper_case,
                                                       bool preposition,
                                                       bool auxiliary) {

  /** Sujet masculin pronom complément : Le/le**/
  if (subject == "poste de police" || subject == "poste avancé" || subject == "centre de recherche"
      || subject == "complexe de lancement" || subject == "bunker" || subject == "Gang du flush royal"
      || subject == "Syndicat du crime" || subject == "Terrible trio" ||
      subject == "president" || subject == "premier ministre" || subject == "maire"
      || subject == "cerveau d'un petit gang"
      || subject == "Jokari" || subject == "Bouffon multicolor"
      || subject == "Mancho") {
    if (auxiliary == 1) {
      if (upper_case == 1) {
        return AUXILIARY_UPPER_CASE[4];
      } else {
        return AUXILIARY_LOWER_CASE[4];
      }
    } else {
      if (preposition == 1) {
        if (upper_case == 1) {
          return PREPOSITION_UPPER_CASE[2];
        } else {
          return PREPOSITION_LOWER_CASE[2];
        }
      } else {
        if (upper_case == 1) {
          return PRONOUN_UPPER_CASE[0];
        } else {
          return PRONOUN_LOWER_CASE[0];
        }
      }
    }
    /**sujet pluriel pronom complément : Les/les**/
  } else if (subject == "civils en danger" || subject == "réfugiés" || subject == "otages"
      || subject == "forces de l'ordre" || subject == "minions" ||
      subject == "malfrats" || subject == "communistes" || subject == "méchants motards"
      || subject == "manifestants contre l'avortement" ||
      subject == "rebelles" || subject == "installations ennemies" || subject == "communications"
      || subject == "Maitres du mal" || subject == "Cavaliers d'Apocalypse" ||
      subject == "Maraudeurs") {
    if (auxiliary == 1) {
      if (upper_case == 1) {
        return AUXILIARY_UPPER_CASE[3];
      } else {
        return AUXILIARY_LOWER_CASE[3];
      }
    } else {
      if (preposition == 1) {
        if (upper_case == 1) {
          return PREPOSITION_UPPER_CASE[3];
        } else {
          return PREPOSITION_LOWER_CASE[3];
        }
      } else {
        if (upper_case == 1) {
          return PRONOUN_UPPER_CASE[2];
        } else {
          return PRONOUN_LOWER_CASE[2];
        }
      }
    }
    /**sujet féminin pronom complément : La/la**/
  } else if (subject == "caserne" || subject == "centrale nucléaire" || subject == "bande d'écolos"
      || subject == "bande de bobos" || subject == "Confrérie du mal"
      || subject == "Société du serpent" || subject == "Gamma Corps" || subject == "Harley Couine" || subject == "Loka"
      || subject == "Magnétaule" || subject == "Etron" || subject == "Mentalo") {
    if (auxiliary == 1) {
      if (upper_case == 1) {
        return AUXILIARY_UPPER_CASE[4];
      } else {
        return AUXILIARY_LOWER_CASE[4];
      }
    } else {
      if (preposition == 1) {
        if (upper_case == 1) {
          return PREPOSITION_UPPER_CASE[1];
        } else {
          return PREPOSITION_LOWER_CASE[1];
        }
      } else {
        if (upper_case == 1) {
          return PRONOUN_UPPER_CASE[1];
        } else {
          return PRONOUN_LOWER_CASE[1];
        }
      }
    }
    /**sujet féminin pronom complément : L'/l'**/
  } else if (subject == "agence" || subject == "aéroport" || subject == "usine d'armement"
      || subject == "usine de produit chimique" || subject == "usine d'armement") {
    if (auxiliary == 1) {
      if (upper_case == 1) {
        return AUXILIARY_UPPER_CASE[4];
      } else {
        return AUXILIARY_LOWER_CASE[4];
      }
    } else {
      if (preposition == 1) {
        if (upper_case == 1) {
          return PREPOSITION_UPPER_CASE[1];
        } else {
          return PREPOSITION_LOWER_CASE[1];
        }
      } else {
        if (upper_case == 1) {
          return PRONOUN_UPPER_CASE[3];
        } else {
          return PRONOUN_LOWER_CASE[3];
        }
      }
    }
    /** sujet masculin pronom indéfini : Un/un**/
  } else if (subject == "homme d'affaires" || subject == "siège" || subject == "repère secret") {
    if (auxiliary == 1) {
      if (upper_case == 1) {
        return AUXILIARY_UPPER_CASE[4];
      } else {
        return AUXILIARY_LOWER_CASE[4];
      }
    } else {
      if (preposition == 1) {
        if (upper_case == 1) {
          return PREPOSITION_UPPER_CASE[1];
        } else {
          return PREPOSITION_LOWER_CASE[1];
        }
      } else {
        if (upper_case == 1) {
          return PRONOUN_UPPER_CASE[4];
        } else {
          return PRONOUN_LOWER_CASE[4];
        }
      }
    }
    /**sujet féminin pronom indéfini : Une/une**/
  } else if (subject == "personne en détresse" || subject == "école" || subject == "base secrète") {
    if (auxiliary == 1) {
      if (upper_case == 1) {
        return AUXILIARY_UPPER_CASE[4];
      } else {
        return AUXILIARY_LOWER_CASE[4];
      }
    } else {
      if (preposition == 1) {
        if (upper_case == 1) {
          return PREPOSITION_UPPER_CASE[1];
        } else {
          return PREPOSITION_LOWER_CASE[1];
        }
      } else {
        if (upper_case == 1) {
          return PRONOUN_UPPER_CASE[5];
        } else {
          return PRONOUN_LOWER_CASE[5];
        }
      }
    }
  } else {
    Exception("Subject not found !");
  }
}

void MissionGenerator::determine_specs_mission(Mission &mission, int level_max, const vector<int> level_occurrences) {

  unsigned int defineDifficulty = 0;
  unsigned int j = 0;

  //Boucle qui permet la définition du niveau de mission
  for (int i = 0; i < level_max; i++) {

    if (level_occurrences[i] != 0) {

      for (int j = 0; j <= level_occurrences.at(i); j++) {

        defineDifficulty = roll_dice_difficulty(level_max);

        if (defineDifficulty == i + 1) {
          break;
        }
      }
    }
  }

  mission.set_difficulty(defineDifficulty);

  mission.set_xp(EXPERIENCE_STEP[defineDifficulty - 1]);
  mission.set_money_profit(MONEY_STEP[defineDifficulty - 1]);
  mission.set_duration(Duration(defineDifficulty));
  mission.set_injury_duration(Duration(defineDifficulty + 1));
}

void MissionGenerator::generate_mission(Mission &mission,
                                        int characters_level_max,
                                        vector<int> characters_level_occurences) {

  string missionTitle;
  string actioning;
  string objective;
  string description;

  unsigned int define = roll_dice_mission();
  unsigned int superMission = roll_dice_super_mission();

  /**
   * Détermine l'objectif et la description de mission
   */

  /** MISSION DE DEFENSE **/
  if (define == 0 || define == 3 || define == 6) {
    //Détermine de façon aléatoire la nature de la mission (ici : verbe de défense donc)
    actioning = ACTION[define];
    objective += actioning;
    missionTitle += actioning;

    if (superMission <= 5 && characters_level_max >= 5) {
      const string star = DEFEND_STAR[roll_dice_mission()];
      missionTitle += " une personne connue";

      //Complete les champs en choisissent les sujets (ex : le président)
      objective += " " + star;
      description = DEFEND_STAR_SPEECH[roll_dice_mission_speech()];
      boost::replace_all(description, "Gang", VILAIN_TEAM[roll_dice_mission_speech()]);

    } else {
      if (roll_dice_mission_alternate() == 0) {

        const string character = DEFEND_CHARACTER[roll_dice_mission()];
        const string gang = VILAIN_TEAM[roll_dice_mission()];

        //Complete les champs en choisissent les sujets (ex : les civils)
        missionTitle += " des civils";
        objective += " " + determine_pronoun_preposition(character, 0, 0, 0) + " " + character;
        description += DEFEND_CHARACTER_SPEECH[roll_dice_mission_speech()];

        //Remplace ici certains terme sur le principe d'un texte à trou
        boost::replace_all(description, "PronomG", determine_pronoun_preposition(gang, 1, 0, 0));
        boost::replace_all(description, "pronomC", determine_pronoun_preposition(character, 0, 1, 0));
        boost::replace_all(description, "auxiliaireC", determine_pronoun_preposition(character, 0, 0, 1));
        boost::replace_all(description, "auxiliaireG", determine_pronoun_preposition(gang, 0, 0, 1));
        boost::replace_all(description, "Gang", gang);
        boost::replace_all(description, "Character", character);

      } else {

        const string corporation = DEFEND_CORPORATION[roll_dice_mission()];
        const string gang = VILAIN_TEAM[roll_dice_mission()];

        missionTitle += " un lieux";
        description += DEFEND_CORPORATION_SPEECH[roll_dice_mission_speech()];

        if (corporation == "centre de recherche" || corporation == "poste de police"
            || corporation == "centre de recherche") {

          boost::replace_all(description, "PronomC", determine_pronoun_preposition(corporation, 1, 0, 0));
          boost::replace_all(description, "other", "");
          boost::replace_all(description, "PronomG", determine_pronoun_preposition(gang, 1, 0, 0));
          boost::replace_all(description, "pronomG", determine_pronoun_preposition(gang, 0, 0, 0));
          boost::replace_all(description, "preposition", determine_pronoun_preposition(corporation, 0, 1, 0));
          boost::replace_all(description, "auxiliaireC", determine_pronoun_preposition(corporation, 0, 0, 1));
          boost::replace_all(description, "auxiliaireG", determine_pronoun_preposition(gang, 0, 0, 1));
          boost::replace_all(description, "Gang", gang);
          boost::replace_all(description, "Corporation", corporation);

        } else {

          boost::replace_all(description, "PronomC", determine_pronoun_preposition(corporation, 1, 0, 0));
          boost::replace_all(description, "other", determine_pronoun_preposition(corporation, 1, 0, 0));
          boost::replace_all(description, "PronomG", determine_pronoun_preposition(gang, 1, 0, 0));
          boost::replace_all(description, "pronomG", determine_pronoun_preposition(gang, 0, 0, 0));
          boost::replace_all(description, "preposition", determine_pronoun_preposition(corporation, 0, 1, 0));
          boost::replace_all(description, "auxiliaireC", determine_pronoun_preposition(corporation, 0, 0, 1));
          boost::replace_all(description, "auxiliaireG", determine_pronoun_preposition(gang, 0, 0, 1));
          boost::replace_all(description, "Gang", gang);
          boost::replace_all(description, "Corporation", corporation);

        }

        objective += " " + determine_pronoun_preposition(corporation, 0, 0, 0) + " " + corporation;

      }
    }
    /** MISSION D'ATTAQUE**/
  } else if (define == 1 || define == 4 || define == 7) {
    actioning += ACTION[define];
    objective += actioning;
    missionTitle += actioning;

    if (superMission <= 10 && characters_level_max >= 6) {
      const string vilain = SUPER_VILAIN[roll_dice_mission()];
      missionTitle += " un Super Vilain";
      objective += " " + vilain + ".";
      description += SUPER_VILAIN_SPEECH[roll_dice_mission_speech()];

      boost::replace_all(description, "prepositionV", determine_pronoun_preposition(vilain, 1, 0, 0));
      boost::replace_all(description, "SuperVilain", vilain);

    } else {
      const string attack = ATTACK_CHARACTER[roll_dice_mission_speech()];

      objective += " " + determine_pronoun_preposition(attack, 0, 0, 0) + " " + attack;
      missionTitle += " des vilains";

      std::array<std::string, 5> a{"le cerveau du gang", "les minions",
                                   "les malfrats", "les rebelles"};
      auto it = std::find_if(begin(a), end(a), [&](const std::string &s) {
        return mission.get_objective().find(s) != std::string::npos;
      });

      if (it == end(a)) {
        const string villain = VILAIN_TEAM[roll_dice_mission()];

        description += ATTACK_CHARACTER_SPEECH[roll_dice_mission_speech()];
        boost::replace_all(description, "Gang", villain);
        boost::replace_all(description, "PronomG", determine_pronoun_preposition(villain, 1, 0, 0));
        boost::replace_all(description, "PronomC", determine_pronoun_preposition(villain, 1, 0, 0));
        boost::replace_all(description, "auxiliaireG", determine_pronoun_preposition(villain, 0, 0, 1));
        boost::replace_all(description, "prepositionG", determine_pronoun_preposition(villain, 0, 1, 0));
        boost::replace_all(description, "auxiliaireC", determine_pronoun_preposition(villain, 0, 0, 1));
        boost::replace_all(description, "Character", attack);

      } else {

        description += ATTACK_CHARACTER_SPEECH[roll_dice_mission_speech()];
        boost::replace_all(description, "Gang", attack);
        boost::replace_all(description, "Character", attack);
        boost::replace_all(description, "auxiliaireC", determine_pronoun_preposition(attack, 0, 0, 1));
        boost::replace_all(description, "auxiliaireG", determine_pronoun_preposition(attack, 0, 0, 1));
        boost::replace_all(description, "prepositionG", determine_pronoun_preposition(attack, 0, 1, 0));
        boost::replace_all(description, "PronomC", determine_pronoun_preposition(attack, 1, 0, 0));
        boost::replace_all(description, "PronomG", determine_pronoun_preposition(attack, 1, 0, 0));
      }
    }
    /** MISSION DE SABOTAGE **/
  } else if (define == 2 || define == 5 || define == 8) {
    actioning += ACTION[define];
    objective += actioning;
    missionTitle += actioning;
    missionTitle += " une position tactique";

    const string corporation = SABOTAGE_CORPORATION[roll_dice_mission()];
    const string villain = VILAIN_TEAM[roll_dice_mission()];

    description += SABOTAGE_CORPORATION_SPEECH[roll_dice_mission_speech()];

    boost::replace_all(description, "PronomC", determine_pronoun_preposition(corporation, 1, 0, 0));
    boost::replace_all(description, "pronomC", determine_pronoun_preposition(corporation, 0, 0, 0));
    boost::replace_all(description, "PronomG", determine_pronoun_preposition(villain, 1, 0, 0));
    boost::replace_all(description, "pronomG", determine_pronoun_preposition(villain, 0, 0, 0));
    boost::replace_all(description, "PronomG", determine_pronoun_preposition(villain, 1, 0, 0));
    boost::replace_all(description, "prepositionG", determine_pronoun_preposition(villain, 0, 1, 0));
    boost::replace_all(description, "auxiliaireG", determine_pronoun_preposition(villain, 0, 0, 1));
    boost::replace_all(description, "Gang", villain);
    boost::replace_all(description, "Complexe", corporation);

    objective += " " + determine_pronoun_preposition(corporation, 0, 0, 0) + " " + corporation;

  } else {
    throw Exception("Mission type is not defined");
  }

  //Définit un id pour la mission généré à l'aide de boost
  mission.set_id(uuid_define());
  mission.set_title(missionTitle);
  mission.set_description(description);
  mission.set_objective(objective);
  auto regions = region_repository->fetch_all();

  if (regions.empty()) {
    throw Exception("Regions can't be empty for the generator to work");
  }

  mission.set_location(regions.at(random_between(0, regions.size() - 1)));
  mission.set_duration(Duration(roll_dice_dateTime()));
  //Détermine le temps d'indisponibilité d'un héro en cas d'échec de la mission
  mission.set_injury_duration(Duration(roll_dice_dateTime()));

  //Determine la difficulté ainsi que les caractéristiques d'une mission en fonction de la difficulté
  determine_specs_mission(mission, characters_level_max, characters_level_occurences);
}

}
