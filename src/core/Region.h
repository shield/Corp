#pragma once

#include <cereal/archives/xml.hpp>
#include <cereal/cereal.hpp>
#include <string>

namespace core {
/**
 * Représente chaque région du jeu.
 */
class Region {
 private:
  /**
   * Identificateur de la région.
   */
  std::string id;
  /**
   * Nom de la région.
   */
  std::string name;

 public:
  const std::string &get_id() const;
  void set_id(const std::string &id);
  const std::string &get_name() const;
  void set_name(const std::string &name);
  bool operator==(const Region &rhs) const;
  bool operator!=(const Region &rhs) const;
  bool operator<(const Region &rhs) const;
  friend std::ostream &operator<<(std::ostream &out, const Region &region);
  template<class Archive>
  void serialize(Archive &ar) {
    ar(CEREAL_NVP(id));
    ar(CEREAL_NVP(name));
  }
};
}  // namespace core
