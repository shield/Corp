#pragma once

#include <cereal/archives/xml.hpp>
#include <cereal/cereal.hpp>
#include <string>
#include <variant>
#include <cereal/optional_nvp.h>

#include "DateTime.h"
#include "Duration.h"
#include "characters/Agent.h"
#include "characters/Hero.h"
#include "characters/character.h"
#include "Region.h"

using namespace std;

namespace core {
using namespace characters;
/**
 * Représente une mission.
 */
class Mission {
 private:
  /**
   * Identifiant unique de la mission (UUID v4)
   */
  string id;

  /**
   * Titre de la mission
   */
  string title;

  /**
   * Description de la mission
   */
  string description;

  /**
   * Objectif de la mission
   */
  string objective;

  /**
   * Profit de la mission si remporté
   */
  unsigned int money_profit = 15;

  /**
   * Gain d'xp si mission remporté
   */
  unsigned int xp = 5;

  /**
   * Durée de la mission
   */
  Duration duration;

  /**
   * Difficulté de la mission
   */
  unsigned int difficulty = 1;

  /**
   * Probabilité de blessure du personnage.
   */
  unsigned int injury_probability;

  /**
   * Temps d'indisponiblité du personnage si blessé
   */
  Duration injury_duration;

  /**
   * Lieu de la mission
   */
  Region location;

  /**
   * Fin de la mission
   */
  DateTime end;

  /**
   * Début de la mission
   */
  DateTime start;


  /**
   * Personnage choisi par le joueur pour accomplir la mission.
   */
  character selected_character;

  /**
   * Si la mission a été acceptée par le joueur
   */
  bool accepted = false;

  /**
   * Si la mission a été gagnée
   */
  bool win = false;

 public:
  template<class Archive>
  void serialize(Archive &ar) {
    ar(CEREAL_NVP(id));
    ar(CEREAL_NVP(title));
    CEREAL_OPTIONAL_NVP(ar, description);
    CEREAL_OPTIONAL_NVP(ar, objective);
    ar(CEREAL_NVP(location));
    CEREAL_OPTIONAL_NVP(ar, money_profit);
    CEREAL_OPTIONAL_NVP(ar, xp);
    CEREAL_OPTIONAL_NVP(ar, difficulty);
    CEREAL_OPTIONAL_NVP(ar, injury_probability);
    CEREAL_OPTIONAL_NVP(ar, accepted);
  }

  /**
   * Renvoie si une mission a été accepté.
   * @return
   */
  bool is_accepted() const;

  /**
   * Renvoie si une mission a été gagné.
   * @return
   */
  bool is_won() const;

  /**
   * Renvoie si une mission est fini.
   * @return
   */
  bool is_finished() const;

  /**
   * Marque la mission comme gagné.
   */
  void do_win();

  /**
   * Accepte une mission.
   * @param character
   */
  void accept(character &character);

  friend std::ostream &operator<<(std::ostream &out, const Mission &mission);
  bool operator==(const Mission &mission) const;

  const character &get_selected_character() const;
  void set_selected_character(const character &selected_character);
  const string &get_id() const;
  void set_id(const string &id);
  const string &get_title() const;
  void set_title(const string &title);
  const string &get_description() const;
  void set_description(const string &description);
  const string &get_objective() const;
  void set_objective(const string &objective);
  unsigned int get_money_profit() const;
  void set_money_profit(unsigned int money_profit);
  unsigned int get_xp() const;
  void set_xp(unsigned int xp);
  const Duration &get_duration() const;
  void set_duration(const Duration &duration);
  unsigned int get_difficulty() const;
  void set_difficulty(unsigned int difficulty);
  unsigned int get_securing_profit() const;
  void set_securing_profit(unsigned int securing_profit);
  unsigned int get_death_probability() const;
  void set_death_probability(unsigned int death_probability);
  unsigned int get_injury_probability() const;
  void set_injury_probability(unsigned int injury_probability);
  const Duration &get_injury_duration() const;
  void set_injury_duration(const Duration &injury_duration);
  const Region &get_location() const;
  void set_location(const Region &location);
  const DateTime &get_end() const;
  void set_end(const DateTime &end);
  const DateTime &get_start() const;
  void set_start(const DateTime &start);
  const DateTime &get_offer_start() const;
  void set_offer_start(const DateTime &offer_start);
};
}  // namespace core
