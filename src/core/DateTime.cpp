#include "DateTime.h"

namespace core {
DateTime::DateTime(unsigned int days) { day = days; }
DateTime::DateTime() { day = 1; }

unsigned int DateTime::get_day() const { return day; }

void DateTime::next_day() {
  day++;
  month_day++;
  if (month_day > DAYS_IN_A_MONTH) {
    month_day = 1;
    month++;
  }
  if (month > MONTHS_IN_A_YEAR) {
    month = 1;
    year++;
  }
}
unsigned int DateTime::get_month_day() const {
  return month_day;
}
unsigned int DateTime::get_year() const {
  return year;
}
unsigned int DateTime::get_month() const {
  return month;
}

}  // namespace core
