#pragma once

#include <stdexcept>

namespace core {
struct Exception : public std::runtime_error {
  /**
   * Gestion des Exceptions ou erreur
   */
  explicit Exception(const std::string &what_) : std::runtime_error(what_) {}
  explicit Exception(const char *what_) : std::runtime_error(what_) {}
};

/**
 * Génère un nombre aléatoire entre `start` et `end`.
 * @param start
 * @param end
 * @return Un nombre aléatoire
 */
unsigned int random_between(unsigned int start, unsigned int end);
}
