#pragma once

#include "Mission.h"
#include "RegionRepository.h"

#include <string>
#include <vector>
#include <map>
#include <boost/shared_ptr.hpp>

namespace core {
/**
 * Ensemble des missions
 */
class MissionRepository {
 private:
  /**
   * Tableau dynamique de missions
   */
  vector<Mission> missions;

  /**
   * Charge des missions à partir d'un fichier
   * @param path
   */
  void load_file(string path);

 public:
  /**
   * Renvoie un tableau de missions
   * @return Un tableau de missions
   */
  vector<Mission> fetch_all() const;

  /**
   * Récupère toutes les missions dont la region est celle donné en paramètre
   * @param region_id
   * @return un tableau de missions
   */
  vector<Mission> fetch_by_region(string region_id) const;

  /**
   * Récupère la mission dont l'id est donné en paramètre
   * @param mission
   * @param id
   * @return si la mission a bien été trouvé
   */
  bool fetch_by_id(Mission &mission, string id) const;

  /**
   * Insère une mission dans l'ensemble
   * @param mission
   */
  void insert(Mission mission);

  /**
   * Met à jour la mission avec l'id donné en paramètre
   * @param id
   * @param mission
   * @return si la mission a bien été mis à jour
   */
  bool update(string id, Mission mission);

  /**
   * Détruit la mission dont l'id est donné en paramètre
   * @param id
   * @return si la mission a bien été détruite
   */
  bool destroy(string id);

  /**
   * Charge depuis un dossier toutes les missions contenu dans des fichiers
   * @param path
   */
  void load(string path);

  /**
   * Charge depuis du `XML` la mission décrite
   * @param ar
   */
  void load_from_xml(cereal::XMLInputArchive &ar);
};
typedef boost::shared_ptr<MissionRepository> spMissionRepository;
}  // namespace core
