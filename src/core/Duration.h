#pragma once

#include <cereal/archives/xml.hpp>
#include <cereal/cereal.hpp>

namespace core {
class Duration {
  /**
  * Représente la durée en jour
  */
 private:
  unsigned int days = 0;

 public:
  Duration() = default;

  /**
   * Creates a duration.
   *
   * @param d
   */
  explicit Duration(unsigned int days);

  /**
   * Get duration in days.
   *
   * @return
   */
  unsigned int get_days() const;

  template <class Archive>
  void serialize(Archive &ar) {
    ar(days);
  }
};
}  // namespace core
