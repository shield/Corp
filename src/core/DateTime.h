#pragma once

#include <cereal/archives/xml.hpp>
#include <cereal/cereal.hpp>
#include <cereal/optional_nvp.h>

namespace core {
class DateTime {
  /**
    * Représente le temps écoulé en jeu
    */
 private:
  /**
   * Jour
   */
  unsigned int day = 1;

  /**
   * Mois
   */
  unsigned int month = 1;

  /**
   * Jour du mois
   */
  unsigned int month_day = 1;

  /**
   * Année
   */
  unsigned int year = 1;

  /**
   * Jour dans un mois
   */
  const static unsigned int DAYS_IN_A_MONTH = 30;

  /**
   * Mois dans une année
   */
  const static unsigned int MONTHS_IN_A_YEAR = 12;

 public:
  DateTime();

  /**
   * Créer une date.
   * @param jours
   */
  explicit DateTime(unsigned int days);

  unsigned int get_day() const;
  unsigned int get_month() const;
  unsigned int get_month_day() const;
  unsigned int get_year() const;

  /**
   * Passe au jour suivant.
   */
  void next_day();

  template<class Archive>
  void serialize(Archive &ar) {
    ar(CEREAL_NVP(day), CEREAL_NVP(month), CEREAL_NVP(year), CEREAL_NVP(month_day));
  };
};
}  // namespace core
