#pragma once

// clang-format off
#include <cereal/archives/xml.hpp>
#include <cereal/cereal.hpp>
#include <cereal/optional_nvp.h>
// clang-format on

#include "Character.h"

namespace core {
namespace characters {
/**
 * Représente un personnage de catégorie "Agent".
 */
class Agent : public Character {
 private:
  /**
   * Caractéristique de l'agent : Compétence de combat.
   */
  unsigned int fighting_skill = 10;

  /**
   * Caractéristique de l'agent : Discrétion.
   */
  unsigned int stealthness = 10;

  /**
   * Caractéristique de l'agent : Négociation.
   */
  unsigned int negociation = 10;

 public:
  /**
   * Modifie les Caractéristiques de l'agent en fonction de son niveau.
   */
  void set_stats();

  /**
   * Modifie le niveau de l'agent en fonction de son expérience aquise et max.
   */
  void level_update();

  template <class Archive>
  void serialize(Archive &ar) {
    CEREAL_OPTIONAL_NVP(ar, name);
    CEREAL_OPTIONAL_NVP(ar, surname);
    CEREAL_OPTIONAL_NVP(ar, description);
    CEREAL_OPTIONAL_NVP(ar, avatar);
    CEREAL_OPTIONAL_NVP(ar, level);
    CEREAL_OPTIONAL_NVP(ar, xp);
    CEREAL_OPTIONAL_NVP(ar, wage);
    CEREAL_OPTIONAL_NVP(ar, fighting_skill);
    CEREAL_OPTIONAL_NVP(ar, destruction_ability);
    CEREAL_OPTIONAL_NVP(ar, stealthness);
    CEREAL_OPTIONAL_NVP(ar, negociation);
  };

  friend std::ostream &operator<<(std::ostream &out, const Agent &agent);
  bool operator==(const Agent &agent) const;
  unsigned int get_fighting_skill() const;
  void set_fighting_skill(unsigned int fighting_skill);
  unsigned int get_stealthness() const;
  void set_stealthness(unsigned int stealthness);
  unsigned int get_negociation() const;
  void set_negociation(unsigned int negociation);
  /**
   * Calcule le cumulé des statistiques de l'agent.
   * @return cumulé des statistiques de l'agent.
   */
  unsigned int get_power() const;
};
}  // namespace characters
}  // namespace core
