#include "Agent.h"
#include "Hero.h"

#include <variant>

namespace core {
namespace characters {
/**
 * Stocke soit un héros soit un agent.
 */
typedef variant<Hero, Agent> character;
}  // namespace characters
}  // namespace core
