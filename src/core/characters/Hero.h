#pragma once

// clang-format off
#include <cereal/archives/xml.hpp>
#include <cereal/cereal.hpp>
#include <cereal/optional_nvp.h>
// clang-format on

#include "Character.h"

namespace core {
namespace characters {
/**
 * Représente un personnage de catégorie "Héros".
 */
class Hero : public Character {
 private:
  /**
   * Caractéristique du héros : Agilité.
   */
  unsigned int agility = 10;

  /**
   * Caractéristique du héros : Charisme.
   */
  unsigned int charisma = 10;

  /**
   * Caractéristique du héros : Ingéniosité.
   */
  unsigned int ingenuity = 10;

 public:
  /**
   * Modifie les Caractéristiques du héros en fonction de son niveau.
   */
  void set_stats();

  /**
   * Modifie le niveau du héros en fonction de son expérience aquise et max.
   */
  void level_update();

  template <class Archive>
  void serialize(Archive &ar) {
    CEREAL_OPTIONAL_NVP(ar, name);
    CEREAL_OPTIONAL_NVP(ar, surname);
    CEREAL_OPTIONAL_NVP(ar, description);
    CEREAL_OPTIONAL_NVP(ar, avatar);
    CEREAL_OPTIONAL_NVP(ar, level);
    CEREAL_OPTIONAL_NVP(ar, xp);
    CEREAL_OPTIONAL_NVP(ar, wage);
    CEREAL_OPTIONAL_NVP(ar, agility);
    CEREAL_OPTIONAL_NVP(ar, destruction_ability);
    CEREAL_OPTIONAL_NVP(ar, charisma);
    CEREAL_OPTIONAL_NVP(ar, ingenuity);
  }

  friend std::ostream &operator<<(std::ostream &out, const Hero &hero);
  bool operator==(const Hero &hero) const;
  unsigned int get_agility() const;
  void set_agility(unsigned int agility);
  unsigned int get_charisma() const;
  void set_charisma(unsigned int charisma);
  unsigned int get_ingenuity() const;
  void set_ingenuity(unsigned int ingenuity);
  /**
   * Calcule le cumulé des statistiques du héros.
   * @return cumulé des statistiques du héros.
   */
  unsigned int get_power() const;
};
}  // namespace characters
}  // namespace core
