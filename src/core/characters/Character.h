#pragma once

#include <string>
#include "../DateTime.h"
#include "../Duration.h"

using namespace std;

namespace core {
namespace characters {
/**
 * Représente un personnage (agent ou héros).
 */
class Character {
 protected:
  /**
   * Prénom du personnage.
   */
  string name;

  /**
   * Nom du personnage.
   */
  string surname;

  /**
   * Description du personnage.
   */
  string description;

  /**
   * Avatar/Illustration du personnage.
   */
  string avatar;

  /**
   * Niveau du personnage, initialisé à 1.
   */
  unsigned int level = 1;

  /**
   * Expérience acquise par le personnage, initialisée à 0.
   */
  unsigned int xp = 0;

  /**
   * Expérience maximale du personnage, initialisée à 10.
   */
  unsigned int xp_max = 10;

  /**
   * Booléen définissant si le personnage est disponible pour une mission ou
   * non, initialisée à Vrai.
   */
  bool available = true;

  /**
   * Date qui définit le début d'une indisponibilité.
   */
  DateTime indisponibility_start;

  /**
   * Durée pendant laquelle le personnage est indisponible.
   */
  Duration indisponibility_duration;

  /**
   * Salaire du personnage (par mois).
   */
  unsigned int wage = 10;

  /**
   * Caractéristique du personnage : Capacité de destruction.
   */
  unsigned int destruction_ability = 10;

 public:
  friend std::ostream &operator<<(std::ostream &out,
                                  const Character &character);
  bool operator==(const Character &character) const;
  const string &get_name() const;
  void set_name(const string &name);
  const string &get_surname() const;
  void set_surname(const string &surname);
  const string &get_description() const;
  void set_description(const string &description);
  unsigned int get_level() const;
  void set_level(unsigned int level);
  unsigned int get_xp() const;
  void set_xp(unsigned int xp);
  unsigned int get_xp_max() const;
  void set_xp_max(unsigned int xp_max);
  bool get_available() const;
  void set_available(bool available);
  const DateTime &get_indisponibility_start() const;
  void set_indisponibility_start(const DateTime &indisponibility_start);
  const Duration &get_indisponibility_duration() const;
  void set_indisponibility_duration(const Duration &indisponibility_duration);
  unsigned int get_wage() const;
  void set_wage(unsigned int wage);
  unsigned int get_destruction_ability() const;
  void set_destruction_ability(unsigned int destruction_ability);
  const string &get_avatar() const;
  void set_avatar(const string &avatar);
};
}  // namespace characters
}  // namespace core
