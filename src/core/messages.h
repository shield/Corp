#include <string>

using namespace std;

namespace core {
/**
 * Message pour une mission réussie.
 */
const static string SUCCEEDED_MISSION = "Mission %1% réussie";

/**
 * Message pour une mission ratée.
 */
const static string FAILED_MISSION = "Mission %1% ratée";

/**
 * Message pour un personnage bléssé.
 */
const static string CHARACTER_INJURY = "%1% est blessé pendant : %2% jours";

/**
 * Détermine la grammaire d'une description de misison
 */
const string PRONOUN_UPPER_CASE[] = {"Le", "La", "Les", "L'", "Un", "Une"};
const string PRONOUN_LOWER_CASE[] = {"le", "la", "les", "l'", "un", "une"};

const string PREPOSITION_UPPER_CASE[] = {"A", "Á", "Au", "Aux"};
const string PREPOSITION_LOWER_CASE[] = {"a", "à", "au", "aux"};

const string AUXILIARY_UPPER_CASE[] = {"On", "Ont", "Son", "Sont", "Est", "A"};
const string AUXILIARY_LOWER_CASE[] = {"on", "ont", "son", "sont", "est", "a"};

const unsigned int length = 9;
const unsigned int lengthSpeech = 3;

/**
 * Verbes d'actions permettent de déterminer la nature de la mission Respecter ordre (1:Defense, 2:Attaque, 3:Sabotage)
 */
const string ACTION[length] = {"Protéger", "Assassiner", "Infiltrer",
                               "Sauver", "Tuer", "Saboter",
                               "Défendre", "Capturer", "Détruire"};

/**
 * Contient les noms des personnes à défendre dans le cadre d'une mission de défense
 *
 */
const string DEFEND_CHARACTER[length] = {
    "president", "civils en danger", "réfugiés",
    "otages", "premier ministre", "personne en détresse",
    "maire", "forces de l'ordre", "homme d'affaires"};

/**
 * Contient les noms des batiment ou lieux à défendre dans le cadre d'une mission de défense
 *
 */
const string DEFEND_CORPORATION[length] = {"poste de police",
                                           "agence",
                                           "poste avancé",
                                           "aéroport",
                                           "usine d'armement",
                                           "caserne",
                                           "école",
                                           "centre de recherche",
                                           "centrale nucléaire"};

/**
 * Contient les noms des personnes à attaquer dans le cadre d'une mission d'attaque
 */
const string ATTACK_CHARACTER[length] = {
    "cerveau d'un petit gang", "minions",
    "malfrats", "communistes",
    "méchants motards", "manifestants contre l'avortement",
    "rebelles", "bande d'écolos",
    "bande de bobos"};

/**
 * Contient les noms des lieux dans le cadre d'une mission de sabotage
 */
const string SABOTAGE_CORPORATION[length] = {
    "siège", "complexe de lancement",
    "installations ennemies", "base secrète",
    "communications", "bunker",
    "usine de produit chimique", "usine d'armement",
    "repère secret"};

/**
 * Contient les noms des personnages important à défendre dans le cadre d'une mission de défense
 */
const string DEFEND_STAR[length] = {
    "Donald Trompe", "François Grollande", "Barack Obalmasqué",
    "Kim jong deux", "Eddy Malo", "Loïc Cpasautomatik (askip il est Russe ...)",
    "Tony Blaireaux", "Guillaume le bobo (il croit que les ninja existe ...)",
    "Edouard la rage (tout est dans le nom ...)"};

/**
 * Contient les noms des énemies importants à attaquer dans le cadre d'une Super mission
 */
const string SUPER_VILAIN[length] = {"Jokari",
                                     "Harley Couine",
                                     "Loka",
                                     "Magnétaule",
                                     "Bouffon multicolor",
                                     "Etron",
                                     "Mentalo",
                                     "Mancho",
                                     "Doomsyesterday"};

/**
 * Contient les noms des différents gangs pouvant effectuer une mission
 */
const string VILAIN_TEAM[length] = {"Confrérie du mal",
                                    "Gang du flush royal",
                                    "Syndicat du crime",
                                    "Terrible trio",
                                    "Société du serpent",
                                    "Maitres du mal",
                                    "Cavaliers d'Apocalypse",
                                    "Gamma Corps",
                                    "Maraudeurs"};

/**
 * Phrases de description concernant les missions de défense de personne importante
 */
const string DEFEND_STAR_SPEECH[lengthSpeech] = {
    "Une personne importante à besoin de votre aide ! Gang à tendu un piège "
    "! Envoyez une équipe sur le champ !",
    "Un danger aproche ! Une personne importante vous propose un contrat de "
    "protection !",
    "Gang à capturer une personne de grande valeur ! Seul l'agence possède "
    "les ressource nécessaire pour lui venir en aide !"};

/**
 * Phrases de description concernant les missions de défense de personne
 */
const string DEFEND_CHARACTER_SPEECH[lengthSpeech] = {
    "PronomG Gang auxiliaireG en train de tendre un piege ! Envoyez une équipe sur les lieux pour mettre fin à la prise d'otage !",
    "PronomG Gang auxiliaireG bien décidé de passer à l'action ! Venez en aide pronomC Character !",
    "PronomG Gang veulent augmenter leur influence dans la zone ! Empêcher les de "
    "nuire !"};

/**
 * Phrases de description concernant les missions de défense de lieux ou batiment
 */
const string DEFEND_CORPORATION_SPEECH[lengthSpeech] = {
    "PronomC Corporation est attaqué par pronomG Gang ! Envoyez une équipe sur les "
    "lieux afin de sécuriser la zone !",
    "PronomG Gang on décidé de s'attaquer preposition other Corporation ! Empêchez la prise du "
    "batiment ! Envoyez une équipe sur les lieux !",
    "PronomG Gang sous un excès de confiance on décidé de s'attaquer preposition other Corporation ! Montrez leur qui est le patron !"};

/**
 * Phrases de description concernant les missions d'attaque d'énemies
 */
const string ATTACK_CHARACTER_SPEECH[lengthSpeech] = {
    "PronomG Character auxiliaireC en train de réaliser une attaque ! Empêchez les de nuires !",
    "PronomC Character auxiliaireG repéré ! Le moment est "
    "opportunt pour faire baisser leur influence !",
    "PronomG Gang auxiliaireG en train de cambrioler une banque afin d'augmenter leurs "
    "ressource ! C'est le moment de leur assainer un coup fatal !"};

/**
 * Phrases de description concernant les missions de sabotage de lieux
 */
const string SABOTAGE_CORPORATION_SPEECH[lengthSpeech] =
    {"PronomC Complexe du Gang a été repéré ! Il est temps de lancer une opération de sabotage afin d'infliger un "
     "coup dur à l'énemie !", "PronomG Gang possède pronomC Complexe il faut taper là où ça fait mal !",
     "PronomC Complexe "
     "se trouve dans une zone hautement sécurisée appartenant prepositionG Gang! Agir dans le feutré sera plus que nécessaire !"};

/**
 * Phrases de description concernant les super missions
 */
const string SUPER_VILAIN_SPEECH[] =
    {"Un super vilain viens d'apparaitre il te faudrat une sacrée paire de balls ainsi qu'une sacrée équipe pour en venir à bout !",
     "Le monde tremble face prepositionV superVilain ! Il est ton devoir de le sauver !",
     "Bordel ! C'est le chaos dehors ! Il faut que l'agence face quelque chose !"};

}
