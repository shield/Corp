#include "Region.h"

namespace core {
std::ostream &operator<<(std::ostream &out, const Region &region) {
  out << "Region(" << region.get_id() << ", " << region.get_name() << ")";

  return out;
}
const std::string &Region::get_id() const {
  return id;
}
void Region::set_id(const std::string &id) {
  Region::id = id;
}
const std::string &Region::get_name() const {
  return name;
}
void Region::set_name(const std::string &name) {
  Region::name = name;
}
bool Region::operator==(const Region &rhs) const {
  return id == rhs.id;
}
bool Region::operator!=(const Region &rhs) const {
  return !(rhs == *this);
}
bool Region::operator<(const Region &rhs) const {
  if (id < rhs.id)
    return true;
  if (rhs.id < id)
    return false;
}
}  // namespace core
