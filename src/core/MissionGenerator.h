#pragma once

#include "Mission.h"
#include "RegionRepository.h"
#include <string>
#include <vector>

namespace core {
class MissionGenerator {
  /**
 * Génération de mission procédurale.
 */
  spRegionRepository region_repository;
 public:
  explicit MissionGenerator(spRegionRepository &region_repository);

  /**
   * @brief Détermine id d'une mission
   * @return uuid
   */
  string uuid_define();

  /**
   * @brief Détermine le type de mission
   * @return value between 0 - 9
   */
  unsigned int roll_dice_mission();

  /**
   * @brief Détermine une mission alternative
   * @return value between 0 - 1
   */
  unsigned int roll_dice_mission_alternate();

  /**
   * @brief Détermine la phrase de description de mission
   * @return value between 0 - 3
   */
  unsigned int roll_dice_mission_speech();

  /**
   * @brief Détermine si une mission sera super ou non
   * @return value between 0 - 100
   */
  unsigned int roll_dice_super_mission();

  /**
   * @brief Détermine la durée en jours
   * @return value between 0 - 7
   */
  unsigned int roll_dice_dateTime();

  /**
   * @brief Détermine la difficulté d'une mission
   * @return value between 1 - 10
   */
  unsigned int roll_dice_difficulty(int level_max);

  /**
   * @brief Permer de déterminer le pronom et la preposition d'un sujet pour la description des missions
   * @param subject
   * @param upper_case
   * @param preposition
   * @param auxiliary
   * @return pronoun
   */
  string determine_pronoun_preposition(string subject, bool upper_case, bool preposition, bool auxiliary);

  /**
   * @brief Détermine les spécificité d'une mission en fonction du niveaux des héros
   * @param mission
   * @param level_max
   * @param level_occurence
   */
  void determine_specs_mission(Mission &mission, int level_max, vector<int> level_occurrences);

  /**
   * @brief Génère une mission
   * @return une mission
   */
  void generate_mission(Mission &mission,
                        int characters_level_max,
                        vector<int> characters_level_occurences);
};
typedef boost::shared_ptr<MissionGenerator> spMissionGenerator;
}