#include "Game.h"
#include "messages.h"
#include "rules.h"

#include <fstream>
#include <boost/format.hpp>

namespace core {
namespace fs = boost::filesystem;
using namespace std;
using boost::format;
using boost::io::group;

DateTime Game::now;

Game::Game(fs::path save_file) {
  this->save_file = save_file;
  now = DateTime();
  feed = spFeed(new Feed);
  region_repository = spRegionRepository(new RegionRepository);
  mission_repository = spMissionRepository(new MissionRepository);
  character_repository = spCharacterRepository(new CharacterRepository);
  mission_generator = spMissionGenerator(new MissionGenerator(region_repository));
}

string Game::get_name() { return name; }

fs::path Game::get_save_file() { return save_file; }
spCharacterRepository Game::get_character_repository() {
  return character_repository;
}
spRegionRepository Game::get_region_repository() {
  return region_repository;
}

spMissionRepository Game::get_mission_repository() { return mission_repository; }

void Game::generate(string name, string from) {
  fs::path full_path(fs::initial_path<fs::path>());
  full_path = fs::system_complete(fs::path(from));

  this->name = name;

  if (!fs::exists(full_path)) {
    throw Exception("Folder `" + full_path.string() + "` not found.");
  }

  if (!fs::is_directory(full_path)) {
    throw Exception("Given path `" + full_path.string() +
        "` is not a directory.");
  }

  // Load characters from existing ones into the repository
  fs::path characters_path = full_path / "/characters";

  if (!fs::exists(characters_path)) {
    throw Exception("Folder `" + characters_path.string() + "` not found.");
  }

  if (!fs::is_directory(characters_path)) {
    throw Exception("Given path is not a directory.");
  }

  character_repository->load(characters_path.c_str());

  fs::path region_path = full_path / "/regions.xml";

  ifstream regions(region_path.string(), ios::in);

  if (!regions.is_open()) {
    throw Exception("File can't be opened.");
  }

  cereal::XMLInputArchive regions_ar(regions);
  region_repository->load(regions_ar);

  generate_missions(INITIAL_GENERATED_MISSIONS);
}

void Game::save() {
  fs::path full_path(fs::initial_path<fs::path>());
  full_path = fs::system_complete(fs::path(save_file));

  ofstream file(full_path.c_str());
  cereal::XMLOutputArchive ar(file);

  auto characters = character_repository->fetch_all();

  ar(cereal::make_nvp("name", name));
  ar(cereal::make_nvp("now", Game::now));

  ar.setNextName("regions");
  ar.startNode();
  for (auto const &region : region_repository->fetch_all()) {
    ar(cereal::make_nvp("region", region));
  }
  ar.finishNode();

  ar.setNextName("characters");
  ar.startNode();
  for (auto const &character : characters) {
    if (auto agent = get_if<Agent>(&character)) {
      ar(cereal::make_nvp("agent", *agent));
    } else if (auto hero = get_if<Hero>(&character)) {
      ar(cereal::make_nvp("hero", *hero));
    }
  }
  ar.finishNode();

  ar.setNextName("missions");
  ar.startNode();
  for (auto const &mission : mission_repository->fetch_all()) {
    ar(cereal::make_nvp("mission", mission));
  }
  ar.finishNode();

  ar(cereal::make_nvp("wallet", wallet));
}
void Game::load() {
  fs::path full_path(fs::initial_path<fs::path>());
  full_path = fs::system_complete(fs::path(save_file));

  ifstream file(full_path.c_str());
  cereal::XMLInputArchive ar(file);

  ar(cereal::make_nvp("name", name));
  ar(cereal::make_nvp("now", now));

  region_repository->load(ar);

  ar.startNode();
  while (ar.getNodeName() != nullptr) {
    character_repository->load_from_xml(ar);
  }
  ar.finishNode();

  ar.startNode();
  while (ar.getNodeName() != nullptr) {
    mission_repository->load_from_xml(ar);
  }
  ar.finishNode();

  ar(cereal::make_nvp("wallet", wallet));
}
void Game::next_day() {
  now.next_day();
  if (now.get_month_day() == 1) {
    apply_wage();
  }

  clean_missions();
  update_character_state();

  if (random_between(0, 1) == 0) {
    generate_missions(DAILY_GENERATED_MISSIONS);
  }

  // S'il y en a 10 et que l'on a plus d'argent alors déclenche le gameOver.
  if (character_repository->are_all_characters_injured()) {
    if (wallet < 0) {
      set_game_over();
    }
  }

}
bool Game::accept_mission(Mission &mission, character &character) {

  mission.accept(character);

  return get_character_repository()->update(character) &&
      get_mission_repository()->update(mission.get_id(), mission);

}
bool Game::clean_missions() {

  auto missions = mission_repository->fetch_all();

  for (auto const &mission : missions) {
    if (mission.is_finished()) {
      if (mission.is_won()) {
        feed->push(str(format(SUCCEEDED_MISSION) % mission.get_title()));
      } else {
        feed->push(str(format(FAILED_MISSION) % mission.get_title()));
      }
      character c;
      if (auto hero = get_if<Hero>(&mission.get_selected_character())) {
        Hero h;
        character_repository->fetch_hero_by_name(h, hero->get_name());
        if (mission.is_won()) {
          h.set_available(true);
          if (h.get_level() < LEVEL_MAX) {
            h.set_xp(h.get_xp() + mission.get_xp());
          }
          wallet += mission.get_money_profit();
        } else {
          h.set_available(false);
          h.set_indisponibility_start(Game::now);
          h.set_indisponibility_duration(mission.get_injury_duration());
          feed->push(str(format(CHARACTER_INJURY) % h.get_name() % h.get_indisponibility_duration().get_days()));
        }

        c = h;
        character_repository->update(c);
        mission_repository->destroy(mission.get_id());

      } else if (auto agent = get_if<Agent>(&mission.get_selected_character())) {
        Agent a;
        character_repository->fetch_agent_by_name(a, agent->get_name());
        if (mission.is_won()) {
          a.set_available(true);
          if (a.get_level() < LEVEL_MAX) {
            a.set_xp(a.get_xp() + mission.get_xp());
          }
          wallet += mission.get_money_profit();
        } else {
          a.set_available(false);
          a.set_indisponibility_start(Game::now);
          a.set_indisponibility_duration(mission.get_injury_duration());
          feed->push(str(format(CHARACTER_INJURY) % a.get_name() % a.get_indisponibility_duration().get_days()));
        }

        c = a;
        character_repository->update(c);
        mission_repository->destroy(mission.get_id());
      }
    }
  }
}
void Game::update_character_state() {
  auto characters = character_repository->fetch_all();
  for (auto &charac : characters) {
    character c;
    if (auto hero = get_if<Hero>(&charac)) {
      if (hero->get_indisponibility_start().get_day() != 0 && hero->get_indisponibility_duration().get_days() != 0) {
        if (Game::now.get_day()
            >= (hero->get_indisponibility_start().get_day() + hero->get_indisponibility_duration().get_days())
            && wallet > 0) {

          hero->set_available(true);
          hero->set_indisponibility_duration(Duration(0));
          hero->set_indisponibility_start(DateTime(0));

          c = *hero;

          character_repository->update(c);
        }
      }
    } else if (auto agent = get_if<Agent>(&charac)) {
      if (agent->get_indisponibility_start().get_day() != 0 && agent->get_indisponibility_duration().get_days() != 0) {
        if (Game::now.get_day()
            >= (agent->get_indisponibility_start().get_day() + agent->get_indisponibility_duration().get_days())
            && wallet > 0) {

          agent->set_available(true);
          agent->set_indisponibility_duration(Duration(0));
          agent->set_indisponibility_start(DateTime(0));

          c = *agent;

          character_repository->update(c);
        }
      }
    }
  }

}
spMissionGenerator Game::get_mission_generator() {
  return mission_generator;
}
spFeed Game::get_feed() {
  return feed;
}
int Game::get_wallet() {
  return wallet;
}
void Game::generate_missions(int missions) {
  for (int i = 0; i < missions; i++) {
    Mission mission;
    mission_generator->generate_mission(mission,
                                        character_repository->fetch_level_max(),
                                        character_repository->get_level_occurrences());
    mission_repository->insert(mission);
  }
}

void Game::apply_wage() {
  unsigned int test = character_repository->fetch_wage_total();
  wallet -= test;
}

void Game::set_game_over() {
  game_over = true;
}
bool Game::get_game_over() {
  return game_over;
}

}  // namespace core
