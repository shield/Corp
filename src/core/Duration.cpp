#include "Duration.h"

namespace core {
Duration::Duration(unsigned int d) { days = d; }

unsigned int Duration::get_days() const { return days; }
}  // namespace core
