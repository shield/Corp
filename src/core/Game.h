#pragma once

#include "CharacterRepository.h"
#include "RegionRepository.h"
#include "DateTime.h"
#include "MissionRepository.h"
#include "MissionGenerator.h"
#include "characters/character.h"
#include "Feed.h"

#include <boost/filesystem/operations.hpp>
#include <string>

namespace fs = boost::filesystem;

using namespace std;

namespace core {
using namespace characters;
/**
 * Noyau du jeu
 */
class Game {
 private:
  /**
   * Nom de la partie
   */
  string name;
  /**
   * Localisation du fichier de sauvegarde
   */
  fs::path save_file;
  /**
   * Ensemble des regions du jeu
   */
  spRegionRepository region_repository;
  /**
   * Ensemble des personnages
   */
  spCharacterRepository character_repository;
  /**
   * Ensemble des missions
   */
  spMissionRepository mission_repository;
  /**
   * Générateur de mission procédurale
   */
  spMissionGenerator mission_generator;
  /**
   * Fil d'actualité
   */
  spFeed feed;
  /**
   * Argent du joueur pour la partie
   */
  int wallet = 100;
/**
 * Booléen définissant si oui ou non la partie est perdue.
 */
  bool game_over = false;

 public:
  /**
   * Temps dans le jeu
   */
  static DateTime now;

  /**
   * Constructeur d'une partie
   * @param save_file
   */
  explicit Game(fs::path save_file);

  /**
   * Récupère le nom de la partie
   * @return
   */
  string get_name();

  /**
   * Récupère le montant du portefeuille
   * @return
   */
  int get_wallet();

  /**
   * Récupère l'emplacement du fichier de sauvegarde
   * @return
   */
  fs::path get_save_file();
  spFeed get_feed();
  spRegionRepository get_region_repository();
  spCharacterRepository get_character_repository();
  spMissionRepository get_mission_repository();
  spMissionGenerator get_mission_generator();

  /**
   * Passe au jour suivant
   */
  void next_day();

  /**
   * Accepte une mission.
   * @param mission
   * @param character
   * @return Si la mission a bien été accepté
   */
  bool accept_mission(Mission &mission, character &character);

  /**
   * Nettoye les missions finies
   * @return Si le nettoyage c'est bien passé
   */
  bool clean_missions();

  /**
   * Vérifie si un personnage blessé peut revenir en jeu
   */
  void update_character_state();

  /**
   * Génère un nombre donné de mission et les ajoute au MissionRepository
   * @param missions
   */
  void generate_missions(int missions);

  /**
   * Génère une partie.
   */
  void generate(string name, string from);

  /**
   * Sauvegarde la partie.
   */
  void save();

  /**
   * Charge la partie.
   */
  void load();

  /**
   * Enlève au portefeuille la le total de tous les salaires des personnages.
   */
  void apply_wage();

  /**
   * Déclenche la fin de partie (game over).
   */
  void set_game_over();

  /**
   * Vérifie les conditions de défaite.
   * @return Renvoie si les conditions de défaite sont remplies.
   */
  bool get_game_over();
};
typedef boost::shared_ptr<Game> spGame;
}  // namespace core
