#include <array>
#include "rules.h"
#include "helpers.h"

namespace core {

const static std::array<unsigned int, LEVEL_MAX> difficulty_matrix = {
    60, 90, 140, 200, 205, 225, 255, 270, 285, 340
};
unsigned int to_real_difficulty(unsigned int difficulty_level) {
  if (difficulty_level < LEVEL_MIN || difficulty_level > LEVEL_MAX) {
    throw Exception("Cannot convert to real difficulty");
  }

  return difficulty_matrix[difficulty_level - 1];
}
}
