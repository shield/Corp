
#include "SaveManager.h"

#include <boost/algorithm/string.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/format.hpp>
#include <fstream>

using namespace std;
using namespace boost;

namespace fs = boost::filesystem;

namespace core {
using boost::format;
using boost::io::group;
SaveManager::SaveManager(string &directory) {
  fs::path full_path(fs::initial_path<fs::path>());
  this->directory = fs::system_complete(fs::path(directory));
}

fs::path SaveManager::get_directory() const { return directory; }

spGame SaveManager::create(string from) {
  uuids::random_generator generator;
  uuids::uuid uuid = generator();

  fs::path save_file = this->directory / (uuids::to_string(uuid) + ".xml");

  spGame game = spGame(new Game(save_file));

  game->generate(str(format("Sauvegarde %1%") % (get_saves_length() + 1)), from);

  return game;
}

spGame SaveManager::load(Save save) {
  spGame game = spGame(new Game(save.get_path()));
  game->load();
  return game;
}
vector<Save> SaveManager::get_saves() {
  vector<Save> saves;

  fs::directory_iterator end_iter;
  for (fs::directory_iterator dir_itr(directory); dir_itr != end_iter;
       ++dir_itr) {
    if (fs::is_regular_file(dir_itr->status())) {
      Save save(dir_itr->path());
      saves.push_back(save);
    }
  }

  return saves;
}
unsigned int SaveManager::get_saves_length() {
  fs::directory_iterator end_iter;
  unsigned int length = 0;

  for (fs::directory_iterator dir_itr(directory); dir_itr != end_iter;
       ++dir_itr) {
    if (fs::is_regular_file(dir_itr->status())) {
      length += 1;
    }
  }

  return length;
}

const string Save::get_name() const {
  string name;

  ifstream file(path.c_str());
  cereal::XMLInputArchive ar(file);

  ar(cereal::make_nvp("name", name));

  cout << name << endl;

  return name;
}
Save::Save(fs::path path) {
  this->path = path;
}
const fs::path Save::get_path() const {
  return path;
}
}  // namespace core
