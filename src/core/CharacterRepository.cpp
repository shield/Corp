#include "CharacterRepository.h"
#include "rules.h"

#include <boost/filesystem/operations.hpp>
#include <fstream>

using namespace std;

namespace core {
using namespace characters;

namespace fs = boost::filesystem;

vector<character>
CharacterRepository::fetch_all() {
  return characters;
};

vector<Agent> CharacterRepository::fetch_agents() {
  vector<Agent> agents;

  for (auto const &character : characters) {
    if (auto agent = get_if<Agent>(&character)) {
      agents.push_back(*agent);
    }
  }

  return agents;
}

vector<Hero> CharacterRepository::fetch_heros() {
  vector<Hero> heros;

  for (auto const &character : characters) {
    if (auto hero = get_if<Hero>(&character)) {
      heros.push_back(*hero);
    }
  }

  return heros;
}

void CharacterRepository::load_from_xml(cereal::XMLInputArchive &ar) {
  if (strcmp(ar.getNodeName(), "agent") == 0) {
    Agent agent;
    ar(agent);
    characters.emplace_back(agent);
  } else if (strcmp(ar.getNodeName(), "hero") == 0) {
    Hero hero;
    ar(hero);
    characters.emplace_back(hero);
  } else {
    throw Exception("Could not load file.");
  }
}

void CharacterRepository::load_file(string path) {
  std::ifstream file(path, ios::in);

  if (!file.is_open()) {
    throw Exception("File can't be opened.");
  }

  cereal::XMLInputArchive ar(file);

  load_from_xml(ar);
}

void CharacterRepository::load(string path) {
  fs::path full_path(fs::initial_path<fs::path>());
  full_path = fs::system_complete(fs::path(path));

  if (!fs::exists(full_path)) {
    throw Exception("Folder `" + full_path.string() + "` not found.");
  }

  if (!fs::is_directory(full_path)) {
    throw Exception("Given path `" + full_path.string() +
        "` is not a directory.");
  }

  fs::directory_iterator end_iter;
  for (fs::directory_iterator dir_itr(full_path); dir_itr != end_iter;
       ++dir_itr) {
    if (fs::is_regular_file(dir_itr->status())) {
      load_file(dir_itr->path().c_str());
    }
  }
}

unsigned int CharacterRepository::fetch_level_max() {
  unsigned int level, level_max = 1;

  for (auto const &character : characters) {
    if (auto hero = get_if<Hero>(&character)) {
      level = hero->get_level();
      if (level > level_max) { level_max = level; }
    } else if (auto agent = get_if<Agent>(&character)) {
      level = agent->get_level();
      if (level > level_max) { level_max = level; }
    }
  }
  return level_max;
}

vector<int> CharacterRepository::get_level_occurrences() {
  int level;
  vector<int> level_occurrences(10, 0);

  for (auto const &character : characters) {
    if (auto hero = get_if<Hero>(&character)) {
      level = hero->get_level();
      level_occurrences[level - 1]++;
    } else if (auto agent = get_if<Agent>(&character)) {
      level = agent->get_level();
      level_occurrences[level - 1]++;
    }
  }
  return level_occurrences;
}
bool CharacterRepository::update(character &updated_character) {
  bool updated = false;

  for (auto &character : characters) {
    if (auto updated_agent = get_if<Agent>(&updated_character)) {
      if (auto agent = get_if<Agent>(&character)) {
        if (updated_agent->get_name() == agent->get_name()) {
          updated_agent->level_update();
          updated = true;
          *agent = *updated_agent;
        }
      }
    } else if (auto updated_hero = get_if<Hero>(&updated_character)) {
      if (auto hero = get_if<Hero>(&character)) {
        if (updated_hero->get_name() == hero->get_name()) {
          updated_hero->level_update();
          *hero = *updated_hero;
          updated = true;
        }
      }
    }
  }

  return updated;
}
bool CharacterRepository::fetch_hero_by_name(Hero &hero, string name) {
  bool found;

  for (auto const &c : characters) {
    if (auto h = get_if<Hero>(&c)) {
      if (h->get_name() == name) {
        hero = *h;
        found = true;
      }
    }
  }

  return found;
}
bool CharacterRepository::fetch_agent_by_name(Agent &agent, string name) {
  bool found;

  for (auto const &c : characters) {
    if (auto a = get_if<Agent>(&c)) {
      if (a->get_name() == name) {
        agent = *a;
        found = true;
      }
    }
  }

  return found;
}
unsigned int CharacterRepository::fetch_wage_total() {
  unsigned int total = 0;
  for (auto const &character : characters) {
    if (auto hero = get_if<Hero>(&character)) {
      total += hero->get_wage();
    } else if (auto agent = get_if<Agent>(&character)) {
      total += agent->get_wage();
    }
  }
  return total;
}
bool CharacterRepository::are_all_characters_injured() {
  // Parcours tous les personnages et compte le nombre de blessés.
  auto characters = this->fetch_all();
  int injured = 0;
  for (auto const &character : characters) {
    if (auto agent = get_if<Agent>(&character)) {
      if (agent->get_indisponibility_duration().get_days() != 0) {
        injured++;
      }
    } else if (auto hero = get_if<Hero>(&character)) {
      if (hero->get_indisponibility_duration().get_days() != 0) {
        injured++;
      }
    }
  }
  return injured == characters.size();
}
}  // namespace core
