#include "res.h"
#include <map>
#include "oxygine-sound.h"
extern SoundPlayer player;

namespace res {
Resources ui;
typedef std::map<string, ResSound *> Sounds;
Sounds sounds;

void load() {
  ui.loadXML("../data/ui.xml");
  sounds["music"] = ResSound::create("../data/sounds/royalty-free-8bit-music-battle.ogg", false);
}

void free() {
  player.stop();
  ui.free();

  for (Sounds::iterator i = sounds.begin(); i != sounds.end(); ++i) {
    delete i->second;
  }
}

}
void playSound(const string &id) {
  PlayOptions options;
  options.loop(true);
  player.play(res::sounds[id], options);
  log::messageln("playSound called");
}

void toggleSound() {
  if (player.IsPaused()) {
    player.resume();
  } else  {
    player.pause();
  }
}

bool isPlayingSound() {
  return !player.IsPaused();
}
// namespace res
