#include <Actor.h>
#include "AgentDetailsPanel.h"
#include "res.h"
#include "GameScene.h"

void AgentDetailsPanel::clickHandler(Event *) {
  this->detach();
  GameScene::instance->get_popup_manager()->unpopup();
}

void AgentDetailsPanel::init(Agent &agent) {
  setSize(800, 400);

  spSprite background = new Sprite();
  background->setResAnim(res::ui.getResAnim("window"));
  background->attachTo(this);
  background->setSize(this->getSize());
  background->setPosition(0, 0);

  //EXIT
  exit = new Sprite();
  exit->setResAnim(res::ui.getResAnim("exit_button"));
  exit->attachTo(this);
  exit->setSize(20, 20);
  exit->setPosition(this->getWidth() - exit->getWidth(), 0);
  exit->setColor(Color(Color::Red));

  //STYLE
  TextStyle title;
  title.font = res::ui.getResFont("normal");
  title.vAlign = TextStyle::VALIGN_MIDDLE;
  title.hAlign = TextStyle::HALIGN_MIDDLE;
  title.color = Color(Color::Yellow);
  title.fontSize = 18;
  title.alignMiddle();

  TextStyle body;
  body.font = res::ui.getResFont("normal");
  body.vAlign = TextStyle::VALIGN_MIDDLE;
  body.hAlign = TextStyle::HALIGN_LEFT;
  body.multiline = true;
  body.breakLongWords = true;
  body.color = Color(Color::White);
  body.fontSize = 14;
  body.alignMiddle();

  //NAME + LVL
  spTextField name = new TextField;
  name->attachTo(background);
  name->setStyle(title);
  name->setText(agent.get_name() + " " + agent.get_surname() + "\n LVL : " + to_string(agent.get_level()));
  name->setPosition(background->getSize().x / 2, background->getSize().y / 10); //Position trop "brute"

  //DESCRIPTION
  spTextField descr = new TextField;
  descr->attachTo(background);
  descr->setStyle(body);
  descr->setText(agent.get_description());
  descr->setPosition(MARGIN_LEFT, background->getSize().y / 20 + 80);
  descr->setSize(background->getWidth() - MARGIN_LEFT, 0);


  //FIGHTING SKILLS
  spTextField fightingSkill = new TextField;
  fightingSkill->attachTo(background);
  fightingSkill->setStyle(body);
  fightingSkill->setText("Capacité de combat :" + to_string(agent.get_fighting_skill()));
  fightingSkill->setPosition(MARGIN_LEFT, background->getSize().y / 20 + 120);

  //DESTRUCTION ABILITY
  spTextField destructionAbility = new TextField;
  destructionAbility->attachTo(background);
  destructionAbility->setStyle(body);
  destructionAbility->setText("Potentiel de destruction :" + to_string(agent.get_destruction_ability()));
  destructionAbility->setPosition(MARGIN_LEFT, background->getSize().y / 20 + 160);

  //STEALTHNESS
  spTextField stealthness = new TextField;
  stealthness->attachTo(background);
  stealthness->setStyle(body);
  stealthness->setText("Discrétion :" + to_string(agent.get_stealthness()));
  stealthness->setPosition(MARGIN_LEFT + 400, background->getSize().y / 20 + 160);

  //NEGOCIATION
  spTextField negociation = new TextField;
  negociation->attachTo(background);
  negociation->setStyle(body);
  negociation->setText("Négociation :" + to_string(agent.get_negociation()));
  negociation->setPosition(MARGIN_LEFT + 400, background->getSize().y / 20 + 120);

  //XP
  spTextField xp = new TextField;
  xp->attachTo(background);
  xp->setStyle(body);
  xp->setText(to_string(agent.get_xp()) + "/" + to_string(agent.get_xp_max()) + " XP");
  xp->setPosition(background->getSize().x / 2 - 50, background->getSize().y - 100);

  //WAGE
  spTextField wage = new TextField;
  wage->attachTo(background);
  wage->setStyle(body);
  wage->setText("Wage : " + to_string(agent.get_wage()));
  wage->setPosition(background->getSize().x / 2 - 50, background->getSize().y - 50);
}
void AgentDetailsPanel::onAdded2Stage() {
  exit->addClickListener(CLOSURE(this, &AgentDetailsPanel::clickHandler));
}
void AgentDetailsPanel::onRemovedFromStage() {
  exit->removeAllEventListeners();
}
