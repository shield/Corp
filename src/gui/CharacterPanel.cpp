#include "CharacterPanel.h"
#include "AgentItem.h"
#include "HeroItem.h"
#include "res.h"

using namespace ::core::characters;

void CharacterPanel::init(::core::spGame &game) {
  setSize(200, getStage()->getHeight() * 3 / 4);

  view = new ClipRectActor;

  ResAnim *arrow_up = res::ui.getResAnim("arrow_up");
  ResAnim *arrow_down = res::ui.getResAnim("arrow_down");

  up = new Sprite;
  up->setResAnim(arrow_up);
  up->setSize(arrow_up->getSize());
  up->setPosition(getWidth() / 2 - up->getWidth() / 2, 0);
  up->attachTo(this);

  page = new CharacterPanelPage;
  page->attachTo(view);
  page->setPosition(getWidth() / 2 - page->getWidth() / 2, 0);
  page->init(game);

  view->attachTo(this);
  view->setSize(getWidth(), getHeight() - arrow_up->getHeight() - arrow_down->getHeight() * 2);
  view->setPosition(getWidth() / 2 - view->getWidth() / 2, up->getHeight());

  down = new Sprite;
  down->setResAnim(arrow_down);
  down->setSize(arrow_down->getSize());
  down->setPosition(getWidth() / 2 - down->getWidth() / 2, view->getHeight() + down->getHeight());
  down->attachTo(this);
}

void CharacterPanel::update(const UpdateState &us) {
  page->update(us);
}

void CharacterPanel::wheel_up(Event *) {
  float y = page->getY();
  if (y <= 0) {
    y += STEP;
    page->setY(y);
  }
}

void CharacterPanel::wheel_down(Event *) {
  float y = page->getY();
  if (y >= (view->getHeight() - page->getHeight())) {
    y -= STEP;
    page->setY(y);
  }
}
void CharacterPanel::onRemovedFromStage() {
  up->removeAllEventListeners();
  down->removeAllEventListeners();
  removeEventListeners(this);
}
void CharacterPanel::onAdded2Stage() {
  up->addClickListener(CLOSURE(this, &CharacterPanel::wheel_up));
  down->addClickListener(CLOSURE(this, &CharacterPanel::wheel_down));
  addEventListener(TouchEvent::WHEEL_DOWN, CLOSURE(this, &CharacterPanel::wheel_down));
  addEventListener(TouchEvent::WHEEL_UP, CLOSURE(this, &CharacterPanel::wheel_up));
}
