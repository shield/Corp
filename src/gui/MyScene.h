#pragma once
#include "oxygine-flow.h"
#include "oxygine-framework.h"

using namespace oxygine;

DECLARE_SMART(Scene, spScene);

/**
 * Réprésente la scène courante.
 */
class MyScene : public flow::Scene {
 public:

  /**
   * Constructeur de la scène.
   */
  MyScene();

  /**
   * Renvoie la scène actuelle.
   * @return la scène courante.
   */
  spActor getView() const { return view; }

 protected:

  /**
   * Scène courante.
   */
  spActor view;
};
