#pragma once

#include "../core/characters/Hero.h"
#include "HeroDetailsPanel.h"
#include "oxygine-framework.h"
#include "../core/Game.h"

using namespace oxygine;
using namespace ::core::characters;

DECLARE_SMART(HeroItem, spHeroItem);

class HeroItem : public Actor {
 private:
  ResAnim *border;
  spSprite window;
  spSprite background;
  spTextField title;
  Hero hero;
 public:
  void init(const Hero &hero);
  void clickHandler(Event *E);
  void update(const UpdateState &us);
  void draw_border();
};
