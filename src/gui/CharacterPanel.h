#pragma once

#include "../core/Game.h"
#include "oxygine-framework.h"
#include "HeroItem.h"
#include "AgentItem.h"
#include "CharacterPanelPage.h"

using namespace oxygine;

DECLARE_SMART(CharacterPanel, spCharacterPanel);

class CharacterPanel : public ClipRectActor {
 private:
  spCharacterPanelPage page;
  spClipRectActor view;
  void wheel_up(Event *event);
  void wheel_down(Event *event);
  const int STEP = 50;
  spSprite up;
  spSprite down;
  void onAdded2Stage() override;
  void onRemovedFromStage() override;
 public:
  void init(::core::spGame &game);
  void update(const UpdateState &us) override;
};
