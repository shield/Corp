#pragma once

#include "oxygine-framework.h"

using namespace oxygine;

DECLARE_SMART(TimeControlPanel, spTimeControlPanel);

/**
 * Panneau de contrôle de l'écoulement du temps.
 */
class TimeControlPanel : public Actor {
 protected:
  /**
   * Bouton "Avance Normale".
   */
  spSprite resume_button;

  /**
   * Bouton "Avance Rapide".
   */
  spSprite advance_button;

  /**
   * Bouton "Pause".
   */
  spSprite pause_button;

  /**
   * Contenant des boutons.
   */
  spSprite background;

  /**
   * HitBox du bouton pause
   */
  spSprite pause_button_hitbox;

  /**
   * Marges intérieures.
   */
  const int PADDING_TOP = 5;
  const int PADDING_LEFT = 5;

  void onRemovedFromStage() override;
  void onAdded2Stage() override;

 public:

  /**
   * Coefficient qui sera multiplié au nombre de secondes nécessaires à l'écoulement d'un jour.
   */
  float speed = 1;

  /**
   * Initialisation du panneau.
   */
  void init();

  /**
   * Arrêt de l'écoulement du temps sur un évènement.
   * @param E
   */
  void stopSpeed(Event *E);

  /**
   * Ecoulement normal du temps sur un évènement.
   * @param E
   */
  void normalSpeed(Event *E);

  /**
   * Avance rapide du temps sur un évènement.
   * @param E
   */
  void doubleSpeed(Event *E);
};
