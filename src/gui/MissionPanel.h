#pragma once

#include "oxygine-framework.h"
#include "../core/MissionRepository.h"
#include "MissionPanelRegion.h"
#include "MissionPanelPage.h"

using namespace oxygine;
using namespace ::core;

DECLARE_SMART(MissionPanel, spMissionPanel);

class MissionPanel : public Actor {
 private:
  spClipRectActor view;
  spMissionPanelPage page;
  void wheel(Event *event);
 public:
  void init(spMissionRepository &mission_repository, spRegionRepository &region_repository);
};
