#include "oxygine-framework.h"
#include "../core/characters/Agent.h"
#include "../core/characters/Hero.h"
#pragma once

using namespace oxygine;
using namespace ::core;
using namespace ::characters;

class AgentEvent : public Event {
 public:

  enum {
    SELECTED = eventID('A','g','e','n')
  };

  Agent data;

  AgentEvent(eventType k, Agent &agent): Event(k), data(agent) {}
};


class HeroEvent : public Event {
 public:

  enum {
    SELECTED = eventID('H','e','r','o')
  };

  Hero data;

  HeroEvent(eventType k, Hero &hero): Event(k), data(hero) {}
};
