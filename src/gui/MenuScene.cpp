#include "MenuScene.h"
#include "GameButton.h"
#include "GameScene.h"
#include "Menu.h"

using namespace ::core;

spMenuScene MenuScene::instance;

MenuScene::MenuScene() {
  auto window_width = getStage()->getWidth();
  auto window_height = getStage()->getHeight();

  menu = new Menu;
  menu->init();
  menu->attachTo(view);
  menu->setPosition(window_width / 2 - menu->getWidth() / 2, window_height / 2 - menu->getHeight() / 2);

  addBackHandler([this](Event *){
    if (GameScene::instance) {
      flow::show(GameScene::instance);
    }
  });
  addEventListener(EVENT_POST_SHOWING, [=](Event*) {
    menu->update_state();
  });
}
void MenuScene::update() {
  Scene::update();
}
