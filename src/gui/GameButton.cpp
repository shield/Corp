#include "GameButton.h"
#include "res.h"

GameButton::GameButton(const string &txt) {
  ResAnim *border = res::ui.getResAnim("menu_button");
  setSize(border->getSize());

  setResAnim(border);

  TextStyle body;
  body.font = res::ui.getResFont("normal");
  body.vAlign = TextStyle::VALIGN_MIDDLE;
  body.hAlign = TextStyle::HALIGN_MIDDLE;
  body.color = Color(Color::White);
  body.fontSize = 16;

  spTextField text = new TextField;
  text->attachTo(this);
  text->setSize(getSize());
  text->setPosition(0, 0);
  text->setStyle(body);
  text->setText(txt);

  spColorRectSprite sprite = new ColorRectSprite;
  sprite->setSize(getSize());
  sprite->attachTo(this);
  sprite->setColor(Color::Zero);
}
void GameButton::updateButtonState(state s) {
  BaseButton::updateButtonState(s);

  switch (s) {
    case state::PRESSED:
      setColor(Color::Green);
      setAlpha(PRESSED);
      break;
    case state::HOVERED:
      setColor(Color::Green);
      setAlpha(HOVERED);
      break;
    case state::DISABLED:
      setColor(Color::White);
      setAlpha(DISABLED);
      break;
    default:
      setColor(Color::White);
      setAlpha(NORMAL);
  }
}
