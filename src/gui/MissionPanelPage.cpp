#include "MissionPanelPage.h"

void MissionPanelPage::init(spMissionRepository &mission_repository, spRegionRepository &region_repository) {
  for (auto const &region : region_repository->fetch_all()) {
    spMissionPanelRegion panel_region = new MissionPanelRegion;
    panel_region->attachTo(this);
    panel_region->init(mission_repository, region);

    regions.push_back(panel_region);
  }
}

void MissionPanelPage::update(const UpdateState &us) {
  for (const auto &region : regions) {
    region->update(us);
  }
  draw();
}

void MissionPanelPage::draw() {
  int y = 0;
  for (const auto &region : regions) {
    region->setPosition(PADDING, y);
    y += region->getHeight() + PADDING;
  }
  setHeight(y - PADDING);
}
