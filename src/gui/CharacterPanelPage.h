#pragma once

#include "../core/Game.h"
#include "oxygine-framework.h"
#include "HeroItem.h"
#include "AgentItem.h"

using namespace oxygine;

DECLARE_SMART(CharacterPanelPage, spCharacterPanelPage);

class CharacterPanelPage : public Actor {
 private:
  const int PADDING = 10;
  vector<spHeroItem> hero_items;
  vector<spAgentItem> agent_items;
 public:
  void init(::core::spGame &game);
  void update(const UpdateState &us);
};
