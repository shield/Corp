#pragma once
#include "oxygine-framework.h"

using namespace oxygine;
using namespace std;

namespace res {
extern Resources ui;
void load();
void free();
}  // namespace res
/**
 * Lance un son.
 * @param id
 */
void playSound(const string& id);

/**
 * Active ou désactive le son.
 */
void toggleSound();

/**
 * Booléen définissant si un son est jouée ou non.
 * @return état du son actif/inactif.
 */
bool isPlayingSound();
