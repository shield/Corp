#pragma once

#include <variant>
#include "oxygine-framework.h"
#include "../core/Mission.h"
#include "../core/characters/Agent.h"
#include "../core/characters/Hero.h"
#include "../core/MissionRepository.h"

using namespace oxygine;
using namespace ::core;
using namespace ::characters;

DECLARE_SMART(MissionPopup, spMissionPopup);

/**
 *
 * Représente la pop-up contenant les informations d'une mission, et permettant de l'accepter.
 */
class MissionPopup : public Actor {
 private:
  /**
   * Champs textuel contenant le nom du personnage sélectionné.
   */
  spTextField character_selected;

  /**
   * Style appliqué au champs textuel "character_selected";
   */
  TextStyle charSelectStyle;

  /**
   * Défini les actions exécutées lors de l'acceptation d'une mission.
   * @param ev
   */
  void on_accept(Event *ev);

  /**
   * Défini les actions exécutées si un personnage est sélectionné, retourne l'état de réussite ou echec de cette action.
   * @param event
   * @return
   */
  bool on_character_selected(Event *event);

  /**
   * Personnage associé actuellement à la mission.
   */
  character selected_character;

  /**
   * Booléen renvoyant si oui ou non un personnage est sélectionné.
   */
  bool selected = false;

  /**
   * Mission associée à la pop-up.
   */
  Mission mission;

  /**
   * Le répertoire de missions.
   */
  spMissionRepository mission_repository;

  /**
   * Icone de croix noire sur fond rouge pour fermer la pop-up.
   */
  spSprite exit;

  /**
   * Ecoute le clic sur les différents héros.
   */
  int agent_listener;

  /**
   * Ecoute le clic sur les différents agents.
   */
  int hero_listener;
  
  void onAdded2Stage() override;
  void onRemovedFromStage() override;
 public:

  /**
   * Initialisation de la pop-up.
   * @param mission_repository
   * @param mission
   */
  void init(spMissionRepository mission_repository, const Mission &mission);

  /**
   * Défini les actions exécutées lors du clic sur la croix rouge.
   */
  void clickHandler(Event *);

  /**
   * Met à jour l'affichage de la pop-up.
   */
  void update();
};
