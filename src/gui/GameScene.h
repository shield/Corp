#pragma once

#include "CharacterPanel.h"
#include "MyScene.h"
#include "MissionPanel.h"
#include "TimeControlPanel.h"
#include "PopupManager.h"
#include "InfoFeed.h"
#include "MusicController.h"

DECLARE_SMART(GameScene, spGameScene);
class GameScene : public MyScene {
 private:
  spGame game;
  spCharacterPanel character_panel;
  spMissionPanel mission_panel;
  spTimeControlPanel time_control_panel;
  spPopupManager popup_manager;
  spInfoFeed info_feed;
  spMusicController music_controller;
  timeMS time;
  float day_in_ms;

  const int MARGIN = 20;

 public:
  spGame get_game() const;
  spPopupManager get_popup_manager() const;
  static spGameScene instance;
  explicit GameScene(spGame &game);
};
