#include <Actor.h>
#include "HeroDetailsPanel.h"
#include "res.h"
#include "GameScene.h"
#include <oxygine-forwards.h>

void HeroDetailsPanel::clickHandler(Event *) {
  detach();
  GameScene::instance->get_popup_manager()->unpopup();
}

void HeroDetailsPanel::init(Hero &hero) {
  setSize(800, 400);

  spSprite background = new Sprite();
  background->setResAnim(res::ui.getResAnim("window"));
  background->attachTo(this);
  background->setSize(this->getSize());
  background->setPosition(0, 0);

  //EXIT
  exit = new Sprite();
  exit->setResAnim(res::ui.getResAnim("exit_button"));
  exit->attachTo(this);
  exit->setSize(20, 20);
  exit->setPosition(this->getWidth() - exit->getWidth(), 0);
  exit->setColor(Color(Color::Red));

  //STYLE
  TextStyle title;
  title.font = res::ui.getResFont("normal");
  title.vAlign = TextStyle::VALIGN_MIDDLE;
  title.hAlign = TextStyle::HALIGN_MIDDLE;
  title.color = Color(Color::Yellow);
  title.fontSize = 18;
  title.alignMiddle();

  TextStyle body;
  body.font = res::ui.getResFont("normal");
  body.vAlign = TextStyle::VALIGN_MIDDLE;
  body.hAlign = TextStyle::HALIGN_LEFT;
  body.multiline = true;
  body.breakLongWords = true;
  body.color = Color(Color::White);
  body.fontSize = 14;
  body.alignMiddle();

  //NAME + LVL
  spTextField name = new TextField;
  name->attachTo(background);
  name->setStyle(title);
  name->setText(hero.get_name() + " " + hero.get_surname() + "\n LVL : " + to_string(hero.get_level()));
  name->setPosition(background->getSize().x / 2, background->getSize().y / 10); //Position trop "brute"

  //DESCRIPTION
  spTextField descr = new TextField;
  descr->attachTo(background);
  descr->setStyle(body);
  descr->setText(hero.get_description());
  descr->setPosition(MARGIN_LEFT, background->getSize().y / 20 + 80);
  descr->setSize(background->getWidth() - MARGIN_LEFT, 0);

  //AGILITY
  spTextField fightingSkill = new TextField;
  fightingSkill->attachTo(background);
  fightingSkill->setStyle(body);
  fightingSkill->setText("Agilité :" + to_string(hero.get_agility()));
  fightingSkill->setPosition(MARGIN_LEFT, background->getSize().y / 20 + 120);

  //DESTRUCTION ABILITY
  spTextField destructionAbility = new TextField;
  destructionAbility->attachTo(background);
  destructionAbility->setStyle(body);
  destructionAbility->setText("Potentiel de destruction :" + to_string(hero.get_destruction_ability()));
  destructionAbility->setPosition(MARGIN_LEFT, background->getSize().y / 20 + 160);

  //INGENUITY
  spTextField stealthness = new TextField;
  stealthness->attachTo(background);
  stealthness->setStyle(body);
  stealthness->setText("Discrétion :" + to_string(hero.get_ingenuity()));
  stealthness->setPosition(MARGIN_LEFT + 400, background->getSize().y / 20 + 160);

  //CHARISMA
  spTextField negociation = new TextField;
  negociation->attachTo(background);
  negociation->setStyle(body);
  negociation->setText("Négociation :" + to_string(hero.get_charisma()));
  negociation->setPosition(MARGIN_LEFT + 400, background->getSize().y / 20 + 120);

  //XP
  spTextField xp = new TextField;
  xp->attachTo(background);
  xp->setStyle(body);
  xp->setText(to_string(hero.get_xp()) + "/" + to_string(hero.get_xp_max()) + " XP");
  xp->setPosition(background->getSize().x / 2 - 50, background->getSize().y - 100);

  //WAGE
  spTextField wage = new TextField;
  wage->attachTo(background);
  wage->setStyle(body);
  wage->setText("Wage : " + to_string(hero.get_wage()));
  wage->setPosition(background->getSize().x / 2 - 50, background->getSize().y - 50);
}

void HeroDetailsPanel::onAdded2Stage() {
  exit->addClickListener(CLOSURE(this, &HeroDetailsPanel::clickHandler));
}
void HeroDetailsPanel::onRemovedFromStage() {
  exit->removeAllEventListeners();
}
