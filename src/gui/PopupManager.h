#pragma once

#include "oxygine-framework.h"
#include "MissionPopup.h"

using namespace oxygine;

DECLARE_SMART(PopupManager, spPopupManager);

/**
 * Gestionnaire de pop-up.
 */
class PopupManager : public Actor {
 private:
  /**
   * Etat de l'affichage d'une pop-up, initialisé à Faux.
   */
  bool displaying = false;
 public:

  /**
   * Affiche une pop-up et renvoie si oui ou non elle a été affichée avec succès.
   * @param actor
   * @return succès de l'affichage de la pop-up.
   */
  bool popup(spActor actor);

  /**
   * Signifie au Gestionnaire de pop-up que la précédente pop-up a été fermée.
   */
  void unpopup();
};
