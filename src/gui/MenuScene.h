#pragma once

#include "MyScene.h"
#include "Menu.h"

DECLARE_SMART(MenuScene, spMenuScene);
class MenuScene : public MyScene {
 private:
  spMenu menu;
 public:
  static spMenuScene instance;
  MenuScene();
  void update();
};
