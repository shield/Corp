#pragma once

#include "../core/characters/Agent.h"
#include "oxygine-framework.h"
#include "AgentDetailsPanel.h"
#include "../core/Game.h"

using namespace oxygine;
using namespace ::core::characters;

DECLARE_SMART(AgentItem, spAgentItem);

class AgentItem : public Actor {
 private:
  ResAnim *border;
  spSprite background;
  Agent agent;
  spTextField title;
  spSprite window;
 public:
  void init(const Agent &agent);
  void clickHandler(Event *E);
  void update(const UpdateState &us) override;
  void draw_border();
};