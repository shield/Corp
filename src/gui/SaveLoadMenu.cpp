#include "SaveLoadMenu.h"
#include "GameScene.h"

spSaveLoadMenu SaveLoadMenu::instance;

SaveLoadMenu::SaveLoadMenu(spSaveManager save_manager) {
  auto window_width = getStage()->getWidth();
  auto window_height = getStage()->getHeight();

  content = new Actor;
  content->setWidth(100);

  auto saves = save_manager->get_saves();

  int y = 0;

  for (auto const &save : saves) {
    spGameButton button = new GameButton(save.get_name());
    button->setPosition(content->getWidth() / 2 - button->getWidth() / 2, y);
    button->attachTo(content);
    button->addEventListener(TouchEvent::CLICK, [this, save_manager, save](Event *) {
      spGame game = save_manager->load(save);
      GameScene::instance = new GameScene(game);
      finishNoResult();
      flow::show(GameScene::instance);
    });

    buttons.push_back(button);

    y += button->getHeight() + 20;
  }

  content->setHeight(y - 20);
  content->attachTo(view);
  content->setPosition(window_width / 2 - content->getWidth() / 2, window_height / 2 - content->getHeight() / 2);

  addBackHandler([this](Event *event) {
    finishNoResult();
  });
}

void SaveLoadMenu::onRemovedFromStage() {
  for (const auto &button : buttons) {
    button->removeAllEventListeners();
  }
}
