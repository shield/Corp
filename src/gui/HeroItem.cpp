#include "HeroItem.h"
#include "CharacterEvent.h"
#include "res.h"
#include "GameScene.h"

void HeroItem::init(const Hero &hero) {
  setSize(getParent()->getWidth(), 200);

  this->hero = hero;
  const ResAnim *character = res::ui.getResAnim(hero.get_avatar());

  background = new Sprite;
  background->setResAnim(character);
  background->setSize(character->getSize());
  background->attachTo(this);
  background->setPosition(getWidth() / 2 - background->getWidth() / 2, 8);

  window = new Sprite;
  draw_border();
  window->setSize(border->getSize());
  window->attachTo(this);
  window->setPosition(getWidth() / 2 - window->getWidth() / 2, 0);

  TextStyle st;
  st.font = res::ui.getResFont("normal");
  st.vAlign = TextStyle::VALIGN_MIDDLE;
  st.hAlign = TextStyle::HALIGN_MIDDLE;
  st.color = Color(Color::DarkOrange);
  st.fontSize = 15;

  title = new TextField;
  title->attachTo(this);
  title->setText(hero.get_surname());
  title->setStyle(st);
  title->setSize(getWidth(), 0);
  title->setPosition(0, character->getHeight() + 25);

  addEventListener(TouchEvent::CLICK, CLOSURE(this, &HeroItem::clickHandler));
}

void HeroItem::clickHandler(Event *) {
  HeroEvent hero_event(HeroEvent::SELECTED, hero);
  getStage()->dispatchEvent(&hero_event);

  spHeroDetailsPanel hero_panel = new HeroDetailsPanel;
  hero_panel->init(hero);

  GameScene::instance->get_popup_manager()->popup(hero_panel);
}

void HeroItem::update(const UpdateState &us) {
  GameScene::instance->get_game()->get_character_repository()->fetch_hero_by_name(hero, hero.get_name());
  if (hero.get_available()) {
    background->setAlpha(255);
  } else {
    background->setAlpha(25);
  }
  draw_border();
}
void HeroItem::draw_border() {
  if (this->hero.get_level() == 1) {
    border = res::ui.getResAnim("character_border");
  } else if (this->hero.get_level() < 5) {
    border = res::ui.getResAnim("window_hero_bronze");
  } else if (this->hero.get_level() < 8) {
    border = res::ui.getResAnim("window_hero_silver");
  } else {
    border = res::ui.getResAnim("window_hero_gold");
  }
  window->setResAnim(border);
}