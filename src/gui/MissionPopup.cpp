#include "MissionPopup.h"
#include "res.h"
#include "CharacterEvent.h"
#include "GameScene.h"
#include "GameButton.h"

#include <boost/format.hpp>

using boost::format;
using boost::io::group;

void MissionPopup::clickHandler(Event *) {
  detach();
  GameScene::instance->get_popup_manager()->unpopup();
}

void MissionPopup::init(spMissionRepository mission_repository, const Mission &mission) {
  this->mission = mission;
  this->mission_repository = mission_repository;

  setSize(800, 400);
  setPosition(getStage()->getWidth() / 2 - getWidth() / 2, getStage()->getHeight() / 2 - getHeight() / 2);

  spSprite background = new Sprite();
  background->setResAnim(res::ui.getResAnim("window"));
  background->attachTo(this);
  background->setSize(this->getSize());
  background->setPosition(0, 0);

  TextStyle title_st;
  title_st.font = res::ui.getResFont("normal");
  title_st.vAlign = TextStyle::VALIGN_MIDDLE;
  title_st.hAlign = TextStyle::HALIGN_MIDDLE;
  title_st.color = Color(Color::Yellow);
  title_st.fontSize = 25;

  TextStyle body_st;
  body_st.font = res::ui.getResFont("normal");
  body_st.hAlign = TextStyle::HALIGN_MIDDLE;
  body_st.color = Color(Color::White);
  body_st.multiline = true;
  body_st.fontSize = 20;

  spTextField title = new TextField;
  title->attachTo(this);
  title->setSize(getWidth(), 0);
  title->setPosition(0, 30);
  title->setStyle(title_st);
  title->setText(str(format("%1% (niv. %2%) ") % mission.get_objective() % mission.get_difficulty()));

  const int description_padding = 10;

  spTextField description = new TextField;
  description->attachTo(this);
  description->setSize(getWidth(), 0);
  description->setPosition(description_padding, 80);
  description->setStyle(body_st);
  description->setText(mission.get_description());
  description->setWidth(getWidth() - description_padding);

  charSelectStyle.font = res::ui.getResFont("normal");
  charSelectStyle.vAlign = TextStyle::VALIGN_MIDDLE;
  charSelectStyle.hAlign = TextStyle::HALIGN_MIDDLE;
  charSelectStyle.multiline = true;
  charSelectStyle.fontSize = 20;

  character_selected = new TextField;
  character_selected->attachTo(this);
  character_selected->setSize(getSize());
  character_selected->setPosition(0, 10);
  character_selected->setStyle(charSelectStyle);

  spGameButton button = new GameButton("Accepter");
  button->addEventListener(TouchEvent::CLICK, CLOSURE(this, &MissionPopup::on_accept));
  button->attachTo(this);
  button->setPosition(getWidth() - button->getWidth() - 20, getHeight() - button->getHeight() - 20);
  button->setColor(Color::White);
  button->setTouchEnabled(false);

  exit = new Sprite();
  exit->setResAnim(res::ui.getResAnim("exit_button"));
  exit->attachTo(this);
  exit->setSize(20, 20);
  exit->setPosition(this->getWidth() - exit->getWidth(), 0);
  exit->setColor(Color(Color::Red));

  update();
}

void MissionPopup::on_accept(Event *ev) {
  if (!selected) {
    return;
  }

  if (GameScene::instance->get_game()->accept_mission(mission, selected_character)) {
    detach();
    GameScene::instance->get_popup_manager()->unpopup();
  }
}

bool MissionPopup::on_character_selected(Event *event) {
  log::messageln("A character was selected");
  if (event->type == AgentEvent::SELECTED) {
    Agent agent = dynamic_cast<AgentEvent *>(event)->data;
    if (!agent.get_available()) {
      return false;
    }
    agent.set_available(false);
    selected_character = agent;
    selected = true;
  } else if (event->type == HeroEvent::SELECTED) {
    Hero hero = dynamic_cast<HeroEvent *>(event)->data;
    if (!hero.get_available()) {
      return false;
    }
    hero.set_available(false);
    selected_character = hero;
    selected = true;
  }
  if (selected) {
    update();
  }
}

void MissionPopup::update() {
  if (selected) {
    if (auto hero = std::get_if<Hero>(&selected_character)) {
      charSelectStyle.color = Color(Color::Green);
      character_selected->setText("Personnage sélectionné : Héros " + hero->get_name());
      character_selected->setStyle(charSelectStyle);
    } else if (auto agent = std::get_if<Agent>(&selected_character)) {
      charSelectStyle.color = Color(Color::Green);
      character_selected->setText("Personnage sélectionné : Agent " + agent->get_name());
      character_selected->setStyle(charSelectStyle);
    }
  } else {
    charSelectStyle.color = Color(Color::Red);
    character_selected->setText("Veuillez sélectionner un personnage");
    character_selected->setStyle(charSelectStyle);
  }
}
void MissionPopup::onAdded2Stage() {
  log::messageln("MissionPopup add to stage");
  exit->addEventListener(TouchEvent::CLICK, CLOSURE(this, &MissionPopup::clickHandler));
  agent_listener = getStage()->addEventListener(AgentEvent::SELECTED, CLOSURE(this, &MissionPopup::on_character_selected));
  hero_listener = getStage()->addEventListener(HeroEvent::SELECTED, CLOSURE(this,
      &MissionPopup::on_character_selected));
}
void MissionPopup::onRemovedFromStage() {
  log::messageln("MissionPopup removed from stage");
  exit->removeAllEventListeners();
  getStage()->removeEventListener(agent_listener);
  getStage()->removeEventListener(hero_listener);
}
