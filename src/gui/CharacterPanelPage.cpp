#include "CharacterPanelPage.h"
#include "AgentItem.h"
#include "HeroItem.h"
#include "res.h"

#include <variant>

using namespace ::core::characters;

void CharacterPanelPage::init(::core::spGame &game) {
  setWidth(getParent()->getWidth());
  auto characters = game->get_character_repository()->fetch_all();

  float x = 0;
  float y = 0;

  for (auto const &character : characters) {
    if (auto hero = get_if<Hero>(&character)) {
      spHeroItem hero_item = new HeroItem;
      hero_item->attachTo(this);
      hero_item->init(*hero);
      hero_item->setPosition(x, y);
      y += hero_item->getHeight() + PADDING;

      hero_items.push_back(hero_item);
    } else if (auto agent = get_if<Agent>(&character)) {
      spAgentItem agent_item = new AgentItem;
      agent_item->attachTo(this);
      agent_item->init(*agent);
      agent_item->setPosition(x, y);
      y += agent_item->getHeight() + PADDING;

      agent_items.push_back(agent_item);
    }
  }

  setHeight(y - PADDING);
}
void CharacterPanelPage::update(const UpdateState &us) {
  for (auto const &hero_item : hero_items) {
    hero_item->update(us);
  }
  for (auto const &agent_item : agent_items) {
    agent_item->update(us);
  }
}
