#include "PopupManager.h"

bool PopupManager::popup(spActor actor) {
  Vector2 size = getParent()->getSize();

  if (displaying) {
    return false;
  }

  actor->setPosition(size.x / 2 - actor->getWidth() / 2, size.y / 2 - actor->getHeight() / 2);
  actor->attachTo(this);

  displaying = true;
}
void PopupManager::unpopup() {
  displaying = false;
}
