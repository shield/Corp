#include "MissionPanel.h"
#include "res.h"
#include "MissionPopup.h"
#include "GameScene.h"
#include <boost/intrusive_ptr.hpp>

void MissionPanel::init(spMissionRepository &mission_repository, spRegionRepository &region_repository) {
  setSize(300, getStage()->getHeight() * 3 / 4);

  const ResAnim *border = res::ui.getResAnim("region_mission");

  spBox9Sprite window = new Box9Sprite;
  window->setResAnim(border);
  window->setSize(getSize());
  window->attachTo(this);
  window->setPosition(0, 0);

  TextStyle title_st;
  title_st.font = res::ui.getResFont("normal");
  title_st.vAlign = TextStyle::VALIGN_MIDDLE;
  title_st.hAlign = TextStyle::HALIGN_MIDDLE;
  title_st.color = Color(Color::Black);
  title_st.fontSize = 25;

  spTextField title = new TextField;
  title->attachTo(this);
  title->setSize(getWidth(), 70.f);
  title->setPosition(0, 0);
  title->setStyle(title_st);
  title->setText("Missions");

  const int PADDING_TOP = 100;
  const int PADDING_BOTTOM = 10;

  view = new ClipRectActor;
  view->setSize(getWidth(), getHeight() - PADDING_TOP - PADDING_BOTTOM);
  view->setPosition(0, PADDING_TOP);
  view->attachTo(this);

  page = new MissionPanelPage;
  page->init(mission_repository, region_repository);
  page->attachTo(view);
  page->setPosition(0, 0);

  addEventListener(TouchEvent::WHEEL_DOWN, CLOSURE(this, &MissionPanel::wheel));
  addEventListener(TouchEvent::WHEEL_UP, CLOSURE(this, &MissionPanel::wheel));
}

void MissionPanel::wheel(Event *event) {
  const int step = 10;
  float y = page->getY();

  if (event->type == TouchEvent::WHEEL_DOWN && y >= (view->getHeight() - page->getHeight())) {
    y -= step;
  } else if (event->type == TouchEvent::WHEEL_UP && y <= 0) {
    y += step;
  }

  page->setY(y);
}
