#pragma once

#include <oxygine-framework.h>
#include <oxygine-forwards.h>
#include <string>

#include "BaseButton.h"

using namespace oxygine;

DECLARE_SMART(GameButton, spGameButton);
class GameButton : public BaseButton {
 private:
  void updateButtonState(state s) override;
  const unsigned char NORMAL = 255;
  const unsigned char HOVERED = 150;
  const unsigned char PRESSED = 200;
  const unsigned char DISABLED = 120;
 public:
  explicit GameButton(const std::string &txt);
};
