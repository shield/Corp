#pragma once

#include "MyScene.h"
#include "Menu.h"
#include "../core/SaveManager.h"

using namespace ::core;

DECLARE_SMART(SaveLoadMenu, spSaveLoadMenu);

/**
 * Menu gérant les sauvegardes.
 */
class SaveLoadMenu : public MyScene {
 private:
  /**
   * Contenu du menu.
   */
  spActor content;

  /**
   * Tableau des boutons.
   */
  vector<spGameButton> buttons;
  void onRemovedFromStage();
 public:
  /**
   * Instance du menu.
   */
  static spSaveLoadMenu instance;

  /**
   * Constructeur du menu.
   * @param save_manager
   */
  explicit SaveLoadMenu(spSaveManager save_manager);
};
