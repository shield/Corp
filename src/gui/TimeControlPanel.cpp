#include "TimeControlPanel.h"
#include "res.h"

void TimeControlPanel::init() {
  this->setSize(90, 30);

  background = new Sprite();
  background->attachTo(this);
  background->setAnimFrame(res::ui.getResAnim("window_time"));
  background->setSize(this->getSize());
  background->setPosition(0, 0);

  ResAnim *pause_button_frame = res::ui.getResAnim("pause");
  pause_button = new Sprite();
  pause_button->attachTo(this);
  pause_button->setPosition(PADDING_LEFT, PADDING_TOP);
  pause_button->setSize(pause_button_frame->getSize());
  pause_button->setAnimFrame(pause_button_frame);
  pause_button_hitbox = new ColorRectSprite;
  pause_button_hitbox->attachTo(pause_button);
  pause_button_hitbox->setColor(Color::Zero);
  pause_button_hitbox->setSize(pause_button->getSize());

  ResAnim *resume_button_frame = res::ui.getResAnim("resume_active");
  resume_button = new Sprite();
  resume_button->attachTo(this);
  resume_button->setPosition(35, PADDING_TOP);
  resume_button->setAnimFrame(resume_button_frame);

  ResAnim *advance_button_frame = res::ui.getResAnim("accelerate");
  advance_button = new Sprite();
  advance_button->attachTo(this);
  advance_button->setPosition(65, PADDING_TOP);
  advance_button->setAnimFrame(advance_button_frame);
}

void TimeControlPanel::stopSpeed(Event *E) {
  speed = 0;
  pause_button->setAnimFrame(res::ui.getResAnim("pause_active"));
  resume_button->setAnimFrame(res::ui.getResAnim("resume"));
  advance_button->setAnimFrame(res::ui.getResAnim("accelerate"));
}

void TimeControlPanel::doubleSpeed(Event *E) {
  speed = 0.25;
  pause_button->setAnimFrame(res::ui.getResAnim("pause"));
  resume_button->setAnimFrame(res::ui.getResAnim("resume"));
  advance_button->setAnimFrame(res::ui.getResAnim("accelerate_active"));
}

void TimeControlPanel::normalSpeed(Event *E) {
  speed = 1;
  pause_button->setAnimFrame(res::ui.getResAnim("pause"));
  resume_button->setAnimFrame(res::ui.getResAnim("resume_active"));
  advance_button->setAnimFrame(res::ui.getResAnim("accelerate"));
}
void TimeControlPanel::onRemovedFromStage() {
  pause_button->removeAllEventListeners();
  resume_button->removeAllEventListeners();
  advance_button->removeAllEventListeners();
}
void TimeControlPanel::onAdded2Stage() {
  advance_button->addEventListener(TouchEvent::CLICK, CLOSURE(this, &TimeControlPanel::doubleSpeed));
  pause_button_hitbox->addEventListener(TouchEvent::CLICK, CLOSURE(this, &TimeControlPanel::stopSpeed));
  resume_button->addEventListener(TouchEvent::CLICK, CLOSURE(this, &TimeControlPanel::normalSpeed));
}
