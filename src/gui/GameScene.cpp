#include "GameScene.h"
#include "../core/SaveManager.h"
#include "res.h"
#include "Tiled.h"
#include "MenuScene.h"

#include <boost/format.hpp>

using boost::format;
using boost::io::group;

spGameScene GameScene::instance;

GameScene::GameScene(spGame &g) {
  this->game = g;

  spMissionRepository mission_repository = game->get_mission_repository();
  spRegionRepository region_repository = game->get_region_repository();

  auto window_width = getStage()->getWidth();
  auto window_height = getStage()->getHeight();

  spSprite background = new Sprite;
  background->setAnimFrame(res::ui.getResAnim("background"));
  background->setSize(window_width, window_height);
  background->attachTo(view);

  spFeed feed = game->get_feed();

  const float MAP_SCALE = 0.8;

  spTiled tiled = new Tiled;
  tiled->setScale(MAP_SCALE, MAP_SCALE);
  tiled->init("../data/map.tmx");
  tiled->attachTo(view);
  tiled->setPosition(window_width / 2 - tiled->getScaledWidth() / 2, window_height / 2 - tiled->getScaledHeight() / 2);

  character_panel = new CharacterPanel;
  character_panel->init(game);
  character_panel->setPosition(window_width - character_panel->getWidth() - MARGIN,
                               window_height - character_panel->getHeight() - MARGIN);
  character_panel->attachTo(view);

  info_feed = new InfoFeed;
  info_feed->attachTo(view);
  info_feed->init(feed);
  info_feed->setPosition(window_width / 2 - info_feed->getWidth() / 2, window_height - info_feed->getHeight() - MARGIN);

  mission_panel = new MissionPanel;
  mission_panel->init(mission_repository, region_repository);
  mission_panel->setPosition(MARGIN, window_height - mission_panel->getHeight() - MARGIN);
  mission_panel->attachTo(view);

  time_control_panel = new TimeControlPanel;
  time_control_panel->attachTo(view);
  time_control_panel->setPosition(view->getSize().x - 90, 0);
  time_control_panel->init();

  music_controller = new MusicController;
  music_controller->attachTo(view);
  music_controller->setPosition(view->getSize().x - 125, 5);
  music_controller->init();

  TextStyle body;
  body.font = res::ui.getResFont("normal");
  body.vAlign = TextStyle::VALIGN_MIDDLE;
  body.hAlign = TextStyle::HALIGN_LEFT;
  body.multiline = true;
  body.breakLongWords = true;
  body.color = Color(Color::Black);
  body.fontSize = 14;
  body.alignMiddle();

  TextStyle gameOverStyle;
  gameOverStyle.font = res::ui.getResFont("normal");
  gameOverStyle.vAlign = TextStyle::VALIGN_MIDDLE;
  gameOverStyle.hAlign = TextStyle::HALIGN_MIDDLE;
  gameOverStyle.color = Color(Color::Red);
  gameOverStyle.fontSize = 30;

  spSprite moneyIcon = new Sprite;
  moneyIcon->setAnimFrame(res::ui.getResAnim("dollar"));
  moneyIcon->setPosition(view->getSize().x - 120, 90);
  moneyIcon->attachTo(view);

  spSprite timeIcon = new Sprite;
  timeIcon->setAnimFrame(res::ui.getResAnim("clock"));
  timeIcon->setPosition(view->getSize().x - 245, 45);
  timeIcon->attachTo(view);

  spTextField dayCount = new TextField;
  dayCount->attachTo(view);
  dayCount->setSize(0, 0);
  dayCount->setPosition(view->getSize().x - 220, 50);
  dayCount->setStyle(body);
  dayCount->setText(
      "Annee : " + to_string(game->now.get_year()) + " Mois : " + to_string(game->now.get_month()) + " Jour : "
          + to_string(game->now.get_month_day()));

  spTextField wallet = new TextField;
  wallet->attachTo(view);
  wallet->setSize(0, 0);
  wallet->setPosition(view->getSize().x - 100, 100);
  wallet->setStyle(body);
  wallet->setText(to_string(game->get_wallet()));

  popup_manager = new PopupManager;
  popup_manager->attachTo(view);

  // Game Actor would have own Clock.
  // clock is internal time of each Actor
  // by default own clock has only Stage
  // clock could be paused and all children of this Actor would be paused to
  time = getTimeMS();

  view->setClock(new Clock);
  view->setCallbackDoUpdate([this, dayCount, wallet, window_width, window_height, gameOverStyle](const UpdateState &us) {
    day_in_ms = 2000 * time_control_panel->speed;
    if (us.time > time + day_in_ms && day_in_ms != 0) {
      // Mise à jour de tous les éléments.
      time = us.time;
      game->next_day();
      character_panel->update(us);
      info_feed->update(us);
      dayCount->setText(
          "Annee : " + to_string(game->now.get_year()) + " Mois : " + to_string(game->now.get_month()) + " Jour : "
              + to_string(game->now.get_month_day()));
      wallet->setText(to_string(game->get_wallet()));
      if (game->get_game_over()) {
        spTextField gameOver = new TextField;
        gameOver->attachTo(view);
        gameOver->setSize(0, 0);
        gameOver->setPosition(window_width / 2 - gameOver->getWidth() / 2,
                              window_height / 2 - gameOver->getHeight() - MARGIN);
        gameOver->setStyle(gameOverStyle);
        gameOver->setText("GAME OVER");
        gameOver->update(us);
      }
    }
  });
  addBackHandler([this](Event *event) {
    finishNoResult();
  });
}
spPopupManager GameScene::get_popup_manager() const {
  return popup_manager;
}
spGame GameScene::get_game() const {
  return game;
}
