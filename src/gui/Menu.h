#pragma once

#include <oxygine-framework.h>
#include "GameButton.h"
#include "../core/SaveManager.h"

using namespace oxygine;
using namespace ::core;

DECLARE_SMART(Menu, spMenu);
class Menu : public Actor {
 private:
  spGameButton continue_button;
  spGameButton save_button;
  spGameButton load_game_button;
  spGameButton new_game_button;
 public:
  void update_state();
  void init();
};
