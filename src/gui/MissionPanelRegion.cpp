#include "MissionPanelRegion.h"
#include "MissionPopup.h"
#include "res.h"
#include "GameScene.h"

void MissionPanelRegion::init(spMissionRepository &mission_repository, Region region) {
  this->region = region;
  this->mission_repository = mission_repository;

  TextStyle subtitle_st;
  subtitle_st.font = res::ui.getResFont("normal");
  subtitle_st.vAlign = TextStyle::VALIGN_MIDDLE;
  subtitle_st.hAlign = TextStyle::HALIGN_LEFT;
  subtitle_st.color = Color(Color::Black);
  subtitle_st.fontSize = 20;

  spTextField text = new TextField;
  text->attachTo(this);
  text->setSize(getWidth(), 0.f);
  text->setPosition(0, 0);
  text->setStyle(subtitle_st);
  text->setText(region.get_name());

  for (auto const &mission : mission_repository->fetch_by_region(region.get_id())) {
    add_mission(mission);
  }

  draw();
}

void MissionPanelRegion::draw() {
  int y = 20;
  for (auto text = begin(fields); text != end(fields); text++) {
    Mission mission;

    if (mission_repository->fetch_by_id(mission, text->first)) {
      if (mission.is_accepted()) {
        text->second->setStyleColor(Color(Color::Green));
        text->second->removeAllEventListeners();
      }

      text->second->setPosition(0, y);
      y += 20;
    } else {
      text->second->detach();
      fields.erase(text);
    }
  }

  setSize(getParent()->getWidth(), (fields.size() + 1) * 20);
}

void MissionPanelRegion::update(const UpdateState &us) {
  for (auto const &mission : mission_repository->fetch_by_region(region.get_id())) {
    if (fields.find(mission.get_id()) == fields.end()) {
      add_mission(mission);
    }
  }

  draw();
}

void MissionPanelRegion::add_mission(const Mission &mission) {
  TextStyle st;
  st.font = res::ui.getResFont("normal");
  st.vAlign = TextStyle::VALIGN_MIDDLE;
  st.hAlign = TextStyle::HALIGN_LEFT;
  st.color = Color(Color::Black);
  st.fontSize = 13;

  spTextField text = new TextField;
  text->attachTo(this);
  text->setSize(getWidth(), 0.f);
  text->setStyle(st);
  text->setText(mission.get_objective());

  text->addClickListener([mission, this](Event *) {
    spMissionPopup mission_popup = new MissionPopup;
    mission_popup->init(mission_repository, mission);

    GameScene::instance->get_popup_manager()->popup(mission_popup);
  });

  text->addEventListener(TouchEvent::OVER, [=](Event *) {
    log::messageln("Game Mission hovered");
    text->setAlpha(100);
  });

  text->addEventListener(TouchEvent::OUT, [=](Event *) {
    log::messageln("Game Mission out");
    text->setAlpha(255);
  });

  fields.insert(make_pair(mission.get_id(), text));
}
