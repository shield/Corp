#pragma once

#include "../core/characters/Agent.h"
#include "oxygine-framework.h"

using namespace oxygine;
using namespace ::core::characters;

DECLARE_SMART(AgentDetailsPanel, spAgentDetailsPanel);

class AgentDetailsPanel : public Actor {
private:
    spSprite exit;
    Agent agent;
    void onAdded2Stage() override;
    void onRemovedFromStage() override;
    const int MARGIN_LEFT = 40;
public:
    void init(Agent &agent);

    void clickHandler(Event *E);
};
