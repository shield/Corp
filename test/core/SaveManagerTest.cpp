#include "../../src/core/SaveManager.h"

#include <boost/algorithm/string.hpp>
#include <catch.hpp>
#include <fstream>
#include "utils.cpp"

using namespace core;
namespace fs = boost::filesystem;

TEST_CASE("when SaveManager is created") {
  fs::remove_all(TMP_DIR);
  fs::create_directory(TMP_DIR);
  fs::create_directory(TMP_DIR_SAVES);

  SECTION("it should work for constructor/1") {
    SaveManager save_manager(TMP_DIR_SAVES);

    auto game = save_manager.create("../data");

    REQUIRE(save_manager.get_directory().string().find(TMP_DIR_SAVES));
  }
}

TEST_CASE("when SaveManager creates a new save") {
  fs::remove_all(TMP_DIR);
  fs::create_directory(TMP_DIR);
  fs::create_directory(TMP_DIR_SAVES);
  SECTION("it should save new induced state correctly") {
    string save_directory = TMP_DIR_SAVES;
    SaveManager save_manager(save_directory);

    spGame game = save_manager.create("../data");

    game->save();

    Save save(game->get_save_file());

    spGame game2 = save_manager.load(save);

    REQUIRE(game->get_character_repository()->fetch_heros() ==
        game2->get_character_repository()->fetch_heros());
    REQUIRE(game->get_character_repository()->fetch_agents() ==
        game2->get_character_repository()->fetch_agents());
    REQUIRE(game->get_mission_repository()->fetch_all() ==
        game2->get_mission_repository()->fetch_all());
    REQUIRE(game->get_region_repository()->fetch_all() == game2->get_region_repository()->fetch_all());
  }
}
