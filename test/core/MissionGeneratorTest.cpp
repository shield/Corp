#include <catch.hpp>
#include <fstream>
#include "../../src/core/MissionGenerator.h"
#include "../../src/core/CharacterRepository.h"
#include "../../src/core/Game.h"
#include "../../src/core/SaveManager.h"
#include "utils.cpp"

using namespace core;

TEST_CASE("When MissionRepository generate a mission") {
  SECTION("it should create a mission") {
    spRegionRepository region_repository = spRegionRepository(new RegionRepository);

    Region location;
    location.set_name("Europe");
    location.set_id("europe");

    region_repository->insert(location);

    spMissionGenerator mission_generator = spMissionGenerator(new MissionGenerator(region_repository));
    spCharacterRepository characters = spCharacterRepository(new CharacterRepository);

    characters->load("../test/data/characters/");

    Mission mission;
    mission_generator->generate_mission(mission,
                                        characters->fetch_level_max(),
                                        characters->get_level_occurrences());


    REQUIRE(!mission.get_title().empty());
  }
}
