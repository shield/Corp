#include "../../include/catch.hpp"
#include "../../src/core/DateTime.h"

using namespace core;

TEST_CASE("when DateTime is created") {
  SECTION("it should work for constructor/0") {
    DateTime date_time;

    REQUIRE(date_time.get_day() == 1);
  }
  SECTION("it should work for constructor/1") {
    unsigned int day = 5;
    DateTime date_time(day);

    REQUIRE(date_time.get_day() == day);
  }
}
