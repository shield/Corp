#include <catch.hpp>
#include "../../src/core/MissionRepository.h"

using namespace core;

TEST_CASE("when MissionRepository is created") {
  SECTION("it should create a map of region, mission") {
    spRegionRepository region_repository = spRegionRepository(new RegionRepository);
    spMissionRepository mission_repository = spMissionRepository(new MissionRepository);

    Region location;
    location.set_name("Europe");
    location.set_id("europe");

    region_repository->insert(location);

    mission_repository->load("../test/data/mission/");

    auto missions = mission_repository->fetch_by_region(location.get_id());

    REQUIRE(missions.size() == 1);
  }
}

TEST_CASE("when MissionRepository loads Missions") {
  SECTION("it should instanciate a Mission") {
    MissionRepository mission;

    mission.load("../test/data/mission/");

    auto missions = mission.fetch_all();

    REQUIRE(missions.size() == 1);
  }
}   
