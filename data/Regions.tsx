<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Regions" tilewidth="692" tileheight="512" tilecount="8" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="9" type="region">
  <properties>
   <property name="region" value="asia"/>
  </properties>
  <image width="692" height="512" source="countries/asia.png"/>
 </tile>
 <tile id="10" type="region">
  <properties>
   <property name="region" value="europe"/>
  </properties>
  <image width="300" height="260" source="countries/europe.png"/>
 </tile>
 <tile id="11" type="region">
  <properties>
   <property name="region" value="north_africa"/>
  </properties>
  <image width="388" height="216" source="countries/north_africa.png"/>
 </tile>
 <tile id="12" type="region">
  <properties>
   <property name="region" value="north_america"/>
  </properties>
  <image width="352" height="368" source="countries/north_america.png"/>
 </tile>
 <tile id="13" type="region">
  <properties>
   <property name="region" value="oceania"/>
  </properties>
  <image width="292" height="444" source="countries/oceania.png"/>
 </tile>
 <tile id="14" type="region">
  <properties>
   <property name="region" value="south_africa"/>
  </properties>
  <image width="200" height="268" source="countries/south_africa.png"/>
 </tile>
 <tile id="15" type="region">
  <properties>
   <property name="region" value="south_america"/>
  </properties>
  <image width="208" height="388" source="countries/south_america.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="region" value="greenland"/>
  </properties>
  <image width="140" height="156" source="countries/greenland.png"/>
 </tile>
</tileset>
